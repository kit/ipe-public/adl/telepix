# telepix

## Installation

### Miniconda
Miniconda https://docs.conda.io/projects/miniconda/en/latest/

### python modules
```
conda install numpy numba matplotlib pyyaml jupyter notebook
```

### basil 
```
git clone https://github.com/SiLab-Bonn/basil
cd basil
python setup.py develop
``` 

### telepix
```
git clone --recursive https://gitlab.kit.edu/-/ide/project/toko.hirono/telepix
cd telepix
python setup.py develop
```

### vivado

Xilinx https://www.xilinx.com/support/download.html

Genesys2 https://digilent.com/reference/programmable-logic/guides/installing-vivado-and-sdk

1. Edit path-to-telepix/firmware/vivado/make.sh line 13
```
elif
    source /tools/Xilinx/Vivado/2022.2/settings64.sh  <-- fix path
    vivado -mode tcl -source make_${board}.tcl
```
2. Edit path-to-telepix/firmware/vivado/make.telescope.tcl line 16
```
#set basil_dir ....
set basil_dir /home/thirono/Nextcloud-kit/workspace/basil/basil <--- fix path
set firmware_dir [exec python -c "import os; print(os.path.dirname(os.getcwd()))"]
```
3. Compile
```
cd path-to-telepix/firmware/vivado/
./make.sh telescope
```

4. connect jtag-USB then run 
```
./program.sh telescope
```
or 
```
./program.sh gui
```
and program FPGA with GUI