`timescale 1ps / 1ps

module telepix_core(
//local bus
    input wire BUS_CLK,
    inout wire [7:0] BUS_DATA,
    input wire [15:0] BUS_ADD,
    input wire BUS_RD,
    input wire BUS_WR,
    input wire BUS_RST,
//clocks
    input wire CLK20,
    input wire CLK40,
    input wire CLK100,
    input wire CLK200,
    input wire CLK400,
//fifo
    input wire ARB_READY_OUT,
    output wire ARB_WRITE_OUT,
    output wire [31:0] ARB_DATA_OUT,
    input wire FIFO_FULL,
    input wire FIFO_NEAR_FULL,
// Chip
    // data output stream
    output wire TSCkExt,
    output wire CkExt,
    output wire SyncRes,
    output wire CkRef,
    output wire Clk4n,
    input wire DataOut,
    input wire HBOut,
    output wire DigInj,
    output wire Res_n,
    // shift registers
    output wire CSIn,
    output wire CCk1,
    output wire CCk2,
    output wire CRb,
    output wire CLd,
    input wire CSOut,
// PCB
    output wire INJ_LOOP,
    output wire INJ_PMOD,
    input wire INJ_LOOP_BACK,
// LED, trigger, Btn, etc
    output wire [7:0] LED
);

// -------  MODULE ADREESSES  ------- //

localparam PULSE_INJ_BASEADDR = 16'h0100; // addr width 33 incl. debug
localparam PULSE_INJ_HIGHADDR = 16'h0140-1;

localparam PULSE_SYNC_BASEADDR = 16'h0140; //addr-width = 15 
localparam PULSE_SYNC_HIGHADDR = 16'h0180-1;

localparam PULSE_GATE_BASEADDR = 16'h0180; //addr-width = 15 
localparam PULSE_GATE_HIGHADDR = 16'h01C0-1;

localparam TS_INJ_BASEADDR = 16'h01C0;  //addr-width = 12
localparam TS_INJ_HIGHADDR = 16'h0200-1;

localparam GPIO_BASEADDR = 16'h0200;
localparam GPIO_HIGHADDR = 16'h0280-1;

localparam TELEPIX_RX_BASEADDR = 16'h0380;
localparam TELEPIX_RX_HIGHADDR = 16'h0400-1;

localparam SPI_CONF_BASEADDR = 16'h0400;
localparam SPI_CONF_HIGHADDR = 16'h0600-1;

// -------  FPGA VERSION ------- //
localparam VERSION = 8'h03;
reg RD_VERSION;
always@(posedge BUS_CLK)
    if(BUS_ADD == 16'h0000 && BUS_RD)
        RD_VERSION <= 1;
    else
        RD_VERSION <= 0;
assign BUS_DATA = (RD_VERSION) ? VERSION : 8'bz;

// ------- Data Stream ------- //
wire RX_FIFO_READ,RX_FIFO_EMPTY;
wire [31:0] RX_FIFO_DATA;
wire TS_INJ_FIFO_READ, TS_INJ_FIFO_EMPTY;
wire [31:0] TS_INJ_FIFO_DATA;
wire TS_INJ_FIFO_READ_FALL, TS_INJ_FIFO_EMPTY_FALL;
wire [31:0] TS_INJ_FIFO_DATA_FALL;

rrp_arbiter #( 
    .WIDTH(3)
) rrp_arbiter (
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    .WRITE_REQ({ ~RX_FIFO_EMPTY, ~TS_INJ_FIFO_EMPTY, ~TS_INJ_FIFO_EMPTY_FALL}),
    .HOLD_REQ(3'b0),
    .DATA_IN({RX_FIFO_DATA, TS_INJ_FIFO_DATA, TS_INJ_FIFO_DATA_FALL}),
    .READ_GRANT({RX_FIFO_READ, TS_INJ_FIFO_READ, TS_INJ_FIFO_READ_FALL}),
    .READY_OUT(ARB_READY_OUT),
    .WRITE_OUT(ARB_WRITE_OUT),
    .DATA_OUT(ARB_DATA_OUT)
);
// ------- GECCO ------- //
// injection
wire SPI_CLK;
//assign SPI_CLK = CLK20;
clock_divider #(
      .DIVISOR(200) //200=200kHz
) clock_divider_spi (
      .CLK(CLK20),
      .RESET(BUS_RST),
      .CLOCK(SPI_CLK)
);
wire INJ_EXT_START;
pulse_gen_div #( 
    .BASEADDR(PULSE_INJ_BASEADDR),
    .HIGHADDR(PULSE_INJ_HIGHADDR),
    .CLKDV(2),
    .OUTPUT_SIZE(3)
) pulse_gen_div_inj (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .PULSE_CLKDV2x(CLK400),
    .PULSE_CLKDV(CLK200),
    .PULSE_CLK(CLK100),
    .EXT_START(INJ_EXT_START),
    .PULSE({DigInj, INJ_LOOP, INJ_PMOD})
);

wire [63:0] TIMESTAMP;
timestamp_div #(
    .BASEADDR(TS_INJ_BASEADDR),
    .HIGHADDR(TS_INJ_HIGHADDR),
    .IDENTIFIER(8),
    .CLKDV(2),    //CLKW x CLKDV = CLK  100MHz x 2 = 200MHz
    .DIV_WIDTH(8) //8 do not change! no of bits of (CLKDIV * 4) = 8 ()
) timestamp_div_inj (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .CLK2x(CLK400),
    .CLK(CLK200),
    .CLKW(CLK100),
    .DI(INJ_LOOP_BACK),
    .TIMESTAMP_OUT(TIMESTAMP), //timestamp based on CLKW
    .EXT_TIMESTAMP(64'b0),
    .EXT_ENABLE(1'b0),

    .FIFO_READ(TS_INJ_FIFO_READ),
    .FIFO_EMPTY(TS_INJ_FIFO_EMPTY),
    .FIFO_DATA(TS_INJ_FIFO_DATA),

    .FIFO_READ_FALL(TS_INJ_FIFO_READ_FALL),
    .FIFO_EMPTY_FALL(TS_INJ_FIFO_EMPTY_FALL),
    .FIFO_DATA_FALL(TS_INJ_FIFO_DATA_FALL)
);

wire PULSE_SYNC, SYNC_RES, EN_PULSE_SYNC;
pulse_gen #(
    .BASEADDR(PULSE_SYNC_BASEADDR),
    .HIGHADDR(PULSE_SYNC_HIGHADDR)
) pulse_gen_sync (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .PULSE_CLK(~CLK100),
    .EXT_START(TIMESTAMP[23]),
    .PULSE(PULSE_SYNC)
);
assign SyncRes = (PULSE_SYNC & EN_PULSE_SYNC) | SYNC_RES;
wire PULSE_GATE;
assign INJ_EXT_START = PULSE_GATE & PULSE_SYNC;
pulse_gen #(
    .BASEADDR(PULSE_GATE_BASEADDR),
    .HIGHADDR(PULSE_GATE_HIGHADDR)
) pulse_gen_gate (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .PULSE_CLK(CLK100),
    .EXT_START(1'b0),
    .PULSE(PULSE_GATE)
);

// ------- Chip ------- //
wire [2:0] debug;
wire EN_CkExt, EN_CkRef, EN_TSCkExt;
assign TSCkExt = EN_TSCkExt ? CLK20 : 1'b0; 
assign CkExt = EN_CkExt ? CLK100 : 1'b0;  // this should be faster, but the PCB-FPGA does not allow to be faster....
assign CkRef = EN_CkRef ? CLK20 : 1'b0;   //this was 100MHz in GECCO
telepix_rx #(
    .DATA_IDENTIFIER(4'h1),
    .BASEADDR(TELEPIX_RX_BASEADDR),
    .HIGHADDR(TELEPIX_RX_HIGHADDR)
) telepix_rx (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .RX_CLK2x(CLK200),
    .RX_CLKW(CLK20),
    .RX_DATA(DataOut),

    .FPGA_TIMESTAMP(TIMESTAMP[47:0]),

    .FIFO_READ(RX_FIFO_READ),
    .FIFO_EMPTY(RX_FIFO_EMPTY),
    .FIFO_DATA(RX_FIFO_DATA),
    .ALIGNED(debug[0])
);

wire SCLK1, EN_CCk1;
assign CCk1 = EN_CCk1? SCLK1:1'b0;
two_phase_spi #(
    .BASEADDR(SPI_CONF_BASEADDR),
    .HIGHADDR(SPI_CONF_HIGHADDR),
    .MEM_BYTES(106)
) two_phase_spi_conf (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .SPI_CLK(SPI_CLK),
    .SCLK1(SCLK1),
    .SCLK2(CCk2),
    .SDO(CSOut),
    .SDI(CSIn),
    .SRB(CRb),
    .SLD(CLd),
    .EXT_START(1'b0),
    .SEN()
);
// ------- Switches ------- //
wire [15:0] GPIO_OUT;
gpio #( 
    .BASEADDR(GPIO_BASEADDR), 
    .HIGHADDR(GPIO_HIGHADDR),
    .IO_WIDTH(16),
    .IO_DIRECTION(16'hffff)
) gpio (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(GPIO_OUT)
);

assign Res_n = GPIO_OUT[0];
assign EN_CkExt = GPIO_OUT[1];
assign EN_CkRef = GPIO_OUT[2];
assign EN_TSCkExt = GPIO_OUT[3];
assign SYNC_RES = GPIO_OUT[4];
assign EN_PULSE_SYNC = GPIO_OUT[5];
assign EN_CCk1 = GPIO_OUT[6];

// -------  debug  ------- //
assign LED[0] = debug[0];
assign LED[1] = ~debug[1];
assign LED[2] = ~TS_INJ_FIFO_EMPTY;
assign LED[3] = ~TS_INJ_FIFO_EMPTY_FALL;
assign LED[4] = ~RX_FIFO_EMPTY;
assign LED[7] =  GPIO_OUT[2];

endmodule
