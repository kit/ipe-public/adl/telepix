/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved 
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module pulse_gen_div_core #(
    parameter ABUSWIDTH = 16,
    parameter CLKDV = 4, //only 4 and 2 were tested
    parameter OUTPUT_SIZE =2 
) (
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,
    
    input wire PULSE_CLK,
    input wire PULSE_CLKDV,
    input wire PULSE_CLKDV2x,
    input wire EXT_START,
    output wire [OUTPUT_SIZE-1:0] PULSE
);

localparam VERSION = 1;
localparam N_BYTE_DES = (CLKDV*2-1)/8 + 1;  //TODO is this correct??

wire SOFT_RST;
wire START;
reg CONF_EN;
reg [5:0] CONF_EN_OUT;
reg [31:0] CONF_DELAY;
reg [31:0] CONF_WIDTH;
reg [31:0] CONF_REPEAT;
reg [CLKDV*4-1:0] CONF_PHASE_DES;
reg CONF_DONE;
reg [32:0] CNT;

always@(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 1)
            BUS_DATA_OUT <= CLKDV;
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {CONF_EN_OUT, CONF_EN, CONF_DONE};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= CONF_DELAY[7:0];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= CONF_DELAY[15:8];
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= CONF_DELAY[23:16];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= CONF_DELAY[31:24];
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= CONF_WIDTH[7:0];
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= CONF_WIDTH[15:8];
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= CONF_WIDTH[23:16];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= CONF_WIDTH[31:24];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= CONF_REPEAT[7:0];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= CONF_REPEAT[15:8];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= CONF_REPEAT[23:16];
        else if(BUS_ADD == 14)
            BUS_DATA_OUT <= CONF_REPEAT[31:24];
        else if(BUS_ADD==16)
            BUS_DATA_OUT <= CONF_PHASE_DES;
        // debug
        else if(BUS_ADD == 17)
            BUS_DATA_OUT <= CNT[7:0];
        else if(BUS_ADD == 18)
            BUS_DATA_OUT <= CNT[15:8];
        else if(BUS_ADD == 19)
            BUS_DATA_OUT <= CNT[23:16];
        else if(BUS_ADD == 20)
            BUS_DATA_OUT <= CNT[31:24];
        else if(BUS_ADD == 21)
            BUS_DATA_OUT <= {7'b0, CNT[32]};
        else
            BUS_DATA_OUT <= 8'b0;
    end
end

assign SOFT_RST = (BUS_ADD==0 && BUS_WR);
assign START = (BUS_ADD==1 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 0;
        CONF_EN_OUT <= 6'b11_1111;
        CONF_DELAY <= 0;
        CONF_WIDTH <= 0;
        CONF_REPEAT <= 1;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2)
            {CONF_EN_OUT, CONF_EN} <= BUS_DATA_IN[7:1];
        else if(BUS_ADD == 3)
            CONF_DELAY[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 4)
            CONF_DELAY[15:8] <= BUS_DATA_IN;
        else if(BUS_ADD == 5)
            CONF_DELAY[23:16] <= BUS_DATA_IN;
        else if(BUS_ADD == 6)
            CONF_DELAY[31:24] <= BUS_DATA_IN;
        else if(BUS_ADD == 7)
            CONF_WIDTH[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 8)
            CONF_WIDTH[15:8] <= BUS_DATA_IN;
        else if(BUS_ADD == 9)
            CONF_WIDTH[23:16] <= BUS_DATA_IN;
        else if(BUS_ADD == 10)
            CONF_WIDTH[31:24] <= BUS_DATA_IN;
        else if(BUS_ADD == 11)
            CONF_REPEAT[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 12)
            CONF_REPEAT[15:8] <= BUS_DATA_IN;
        else if(BUS_ADD == 13)
            CONF_REPEAT[23:16] <= BUS_DATA_IN;
        else if(BUS_ADD == 14)
            CONF_REPEAT[31:24] <= BUS_DATA_IN;
        else if(BUS_ADD == 16)
            CONF_PHASE_DES <= BUS_DATA_IN;
        //else if(BUS_ADD[7:4]==4'b0001)  TODO
        //    CONF_PHASE[{BUS_ADD[3:0], 3'b111}:{BUS_ADD[3:0], 3'b000}] <= BUS_DATA_IN;
    end
end

wire RST_SYNC;
wire RST_SOFT_SYNC;
cdc_pulse_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(PULSE_CLK), .pulse_out(RST_SOFT_SYNC));
assign RST_SYNC = RST_SOFT_SYNC || BUS_RST;


wire START_SYNC;
cdc_pulse_sync start_pulse_sync (.clk_in(BUS_CLK), .pulse_in(START), .clk_out(PULSE_CLK), .pulse_out(START_SYNC));

wire EXT_START_SYNC;
reg [2:0] EXT_START_FF;
always @(posedge PULSE_CLK) // first stage
begin
    EXT_START_FF[0] <= EXT_START;
    EXT_START_FF[1] <= EXT_START_FF[0];
    EXT_START_FF[2] <= EXT_START_FF[1];
end
assign EXT_START_SYNC = !EXT_START_FF[2] & EXT_START_FF[1];

wire [32:0] LAST_CNT;
assign LAST_CNT = CONF_DELAY + CONF_WIDTH;

reg [31:0] REAPAT_CNT;
always @ (posedge PULSE_CLK) begin
    if (RST_SYNC)
        REAPAT_CNT <= 0;
    else if(START_SYNC || (EXT_START_SYNC && CONF_EN))
        REAPAT_CNT <= CONF_REPEAT;
    else if(REAPAT_CNT != 0 && CNT == 1)
        REAPAT_CNT <= REAPAT_CNT - 1;
end

always @ (posedge PULSE_CLK) begin
    if (RST_SYNC)
        CNT <= 0; //IS THIS RIGHT?
    else if(START_SYNC || (EXT_START_SYNC && CONF_EN))
        CNT <= 1;
    else if(CNT == LAST_CNT && REAPAT_CNT != 0)
        CNT <= 1;
    else if(CNT == LAST_CNT && CONF_REPEAT==0)
        CNT <= 1;
    else if(CNT == LAST_CNT && REAPAT_CNT == 0)
        CNT <= 0;
    else if(CNT != 0)
        CNT <= CNT + 1;
end

reg [CLKDV*4-1:0] PULSE_DES;
always @ (posedge PULSE_CLK) begin
    if(RST_SYNC || START_SYNC || (EXT_START_SYNC && CONF_EN)) begin
        PULSE_DES <= 0;
    end
    else if(CNT == CONF_DELAY && CNT > 0) begin
        PULSE_DES <= CONF_PHASE_DES;
    end
    else if(CNT == CONF_DELAY+1) begin
        PULSE_DES <= {(CLKDV*4){1'b1}};
    end
    else if(CNT == LAST_CNT) begin
        PULSE_DES <= 0;
    end
end

reg [7:0] byte2cnt;
always @ (posedge PULSE_CLKDV) begin
    if (RST_SYNC)
        byte2cnt <= 0;
    else if (START_SYNC || (EXT_START_SYNC && CONF_EN) || (CNT!=0 & byte2cnt==1))
        byte2cnt <= CLKDV;
    else if (byte2cnt!=0)
        byte2cnt <= byte2cnt-1;
    else
        byte2cnt <= 0;
end

wire [3:0] PULSE_DES_DIV [CLKDV:0];
assign PULSE_DES_DIV[0] = 4'b0;
genvar j;
generate
for (j=0; j<CLKDV; j=j+1)
    assign PULSE_DES_DIV[j+1] = PULSE_DES[j*4+3:j*4];
endgenerate

genvar i;
generate
for (i=0; i<OUTPUT_SIZE; i=i+1) begin
    OSERDESE2 # (
        .DATA_RATE_OQ("DDR"),
        .DATA_WIDTH(4),
        .SERDES_MODE("MASTER")
    ) i_OSERDESE2 (
        .OQ(PULSE[i]),
        .OFB(),
        .TQ(),
        .TFB(),
        .SHIFTOUT1(),
        .SHIFTOUT2(),
        .CLK(PULSE_CLKDV2x),
        .CLKDIV(PULSE_CLKDV),
        .D1(PULSE_DES_DIV[byte2cnt][3]),
        .D2(PULSE_DES_DIV[byte2cnt][2]),
        .D3(PULSE_DES_DIV[byte2cnt][1]),
        .D4(PULSE_DES_DIV[byte2cnt][0]),
        .D5(),
        .D6(),
        .D7(),
        .D8(),
        .TCE(0),
        .OCE(CONF_EN_OUT[i]),
        .TBYTEIN(),
        .TBYTEOUT(),
        .RST(RST_SYNC),
        .SHIFTIN1(),
        .SHIFTIN2(),
        .T1(0),
        .T2(0),
        .T3(0),
        .T4(0)
    );
end
endgenerate

wire DONE;
assign DONE = (CNT == 0);

wire DONE_SYNC;
cdc_pulse_sync done_pulse_sync (.clk_in(PULSE_CLK), .pulse_in(DONE_SYNC), .clk_out(BUS_CLK), .pulse_out(DONE_SYNC));

wire EXT_START_SYNC_BUS;
cdc_pulse_sync ex_start_pulse_sync (.clk_in(PULSE_CLK), .pulse_in(EXT_START && CONF_EN), .clk_out(BUS_CLK), .pulse_out(EXT_START_SYNC_BUS));

always @(posedge BUS_CLK)
    if(RST)
        CONF_DONE <= 1;
    else if(START || EXT_START_SYNC_BUS)
        CONF_DONE <= 0;
    else if(DONE_SYNC)
        CONF_DONE <= 1;

endmodule
