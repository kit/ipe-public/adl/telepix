/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved 
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none
 
module timestamp_div_core
#(
    parameter ABUSWIDTH = 16,
    parameter IDENTIFIER = 4'b0001,
    parameter CLKDV = 2,
    parameter DIV_WIDTH = 8  // TODO do not change!  make this variable and automatically calculates
)(
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    input wire CLK2x, //CLK * 2 = CLK2X e.g. 200MHz x 2 = 400MHz
    input wire CLK,   //CLKW * CLKDV = CLK e.g. 40MHz x 5 = 200MHz
    input wire CLKW,  //40MHz
    input wire DI,
    input wire EXT_ENABLE,
    input wire [63:0] EXT_TIMESTAMP,
    output wire [63:0] TIMESTAMP_OUT,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,
    
    input wire FIFO_READ_FALL,
    output wire FIFO_EMPTY_FALL,
    output wire [31:0] FIFO_DATA_FALL
); 

localparam VERSION = 2;

wire SOFT_RST;
assign SOFT_RST = (BUS_ADD==0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST; 

reg CONF_EN, CONF_EN_FALL, CONF_EXT_ENABLE;
reg CONF_EXT_TIMESTAMP;
reg [7:0] LOST_DATA_CNT;
wire [63:0] TIMESTAMP;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 0;
        CONF_EXT_TIMESTAMP <=0;
        CONF_EXT_ENABLE <= 0;
        CONF_EN_FALL <=0;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2)
            CONF_EN <= BUS_DATA_IN[0];
            CONF_EXT_TIMESTAMP <=BUS_DATA_IN[1];
            CONF_EXT_ENABLE <=BUS_DATA_IN[2];
            CONF_EN_FALL <=BUS_DATA_IN[3];
    end
end

always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 1)
            BUS_DATA_OUT <= CLKDV;
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {2'b0,CONF_EN_FALL, CONF_EXT_ENABLE, CONF_EXT_TIMESTAMP,CONF_EN};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= LOST_DATA_CNT;
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= TIMESTAMP[7:0];
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= TIMESTAMP[15:8];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= TIMESTAMP[23:16];
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= TIMESTAMP[31:24];
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= TIMESTAMP[39:32];
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= TIMESTAMP[47:40];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= TIMESTAMP[55:48];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= TIMESTAMP[63:56];
        else
            BUS_DATA_OUT <= 8'b0;
    end
end

wire RST_SYNC;
wire RST_SOFT_SYNC;
cdc_pulse_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(CLKW), .pulse_out(RST_SOFT_SYNC));
assign RST_SYNC = RST_SOFT_SYNC || BUS_RST;
wire CONF_EN_SYNC, CONF_EN_FALL_SYNC;
assign CONF_EN_SYNC = CONF_EN | (EXT_ENABLE & CONF_EXT_ENABLE);
assign CONF_EN_FALL_SYNC = CONF_EN_FALL | (EXT_ENABLE & CONF_EXT_ENABLE);

reg [7:0] sync_cnt;
always@(posedge BUS_CLK) begin   //what is this?
    if(RST)
        sync_cnt <= 120;
    else if(sync_cnt != 100)
        sync_cnt <= sync_cnt +1;
end 
wire RST_LONG;
assign RST_LONG = sync_cnt[7];

reg [63:0] INT_TIMESTAMP;
always@(posedge CLKW) begin
    if(RST_SYNC)
        INT_TIMESTAMP <= 0;
    else
        INT_TIMESTAMP <= INT_TIMESTAMP + 1;
end
assign TIMESTAMP = CONF_EXT_TIMESTAMP ?  EXT_TIMESTAMP: INT_TIMESTAMP;
assign TIMESTAMP_OUT = TIMESTAMP;

// de-serialize
wire [CLKDV*4-1:0] TDC;
reg  TDC_DES_PREV0;

ddr_des #(.CLKDV(CLKDV)) iddr_des_tdc(.CLK2X(CLK2x), .CLK(CLK), .WCLK(CLKW), .IN(DI), .OUT(TDC), .OUT_FAST());

always @ (posedge CLKW)
    TDC_DES_PREV0 <= TDC[0];
    
wire  [CLKDV*4:0] TDC_TO_COUNT;
assign TDC_TO_COUNT[CLKDV*4] = TDC_DES_PREV0;
assign TDC_TO_COUNT[CLKDV*4-1:0] = TDC;

reg [DIV_WIDTH-1:0] RISING_EDGES_CNT, FALLING_EDGES_CNT;
reg [DIV_WIDTH-1:0] RISING_POS, FALLING_POS;

integer i;
always @ (*) begin
    FALLING_EDGES_CNT = 0;
    RISING_EDGES_CNT = 0;
    RISING_POS = 0;
    FALLING_POS = 0;
    for (i=0; i<CLKDV*4; i=i+1) begin
        if ((TDC_TO_COUNT[CLKDV*4-i-1] == 1) && (TDC_TO_COUNT[CLKDV*4-i]==0)) begin
            if (RISING_EDGES_CNT == 0)
                RISING_POS = i;
                
            RISING_EDGES_CNT = RISING_EDGES_CNT + 1;
        end
        
        if ((TDC_TO_COUNT[i] == 0) && (TDC_TO_COUNT[i+1]==1)) begin
            if (FALLING_EDGES_CNT == 0)
                FALLING_POS = CLKDV*4 - i;
            
            FALLING_EDGES_CNT = FALLING_EDGES_CNT + 1;
        end
    end
end

reg [(63+DIV_WIDTH):0] LAST_RISING;
always@(posedge CLKW)
    if(RST)
        LAST_RISING <= 0;
    else if (RISING_EDGES_CNT > 0 )
        LAST_RISING <= {TIMESTAMP, RISING_POS};

reg [(63+DIV_WIDTH):0] LAST_FALLING;
always@(posedge CLKW)
    if(RST)
        LAST_FALLING <= 0;
    else if (FALLING_EDGES_CNT > 0)
        LAST_FALLING <= {TIMESTAMP, FALLING_POS};

wire RISING, FALLING;
assign RISING = (RISING_EDGES_CNT > 0 & CONF_EN_SYNC);
assign FALLING = (FALLING_EDGES_CNT > 0 & CONF_EN_FALL_SYNC);

reg [2:0] FALLING_FF;
reg [2:0] RISING_FF;
always@(posedge CLKW) begin
    if(RST) begin
        FALLING_FF <= 3'b0;
        RISING_FF <= 3'b0;
    end
    else begin
        FALLING_FF <= {FALLING_FF[1:0], FALLING};
        RISING_FF <= {RISING_FF[1:0], RISING};
    end
end

wire RISING_SYNC, FALLING_SYNC;
assign RISING_SYNC = RISING_FF[0] & ~RISING_FF[1];
assign FALLING_SYNC = FALLING_FF[0] & ~FALLING_FF[1];

wire [(63+DIV_WIDTH):0] cdc_data_in;
assign cdc_data_in =  LAST_RISING;
wire [(63+DIV_WIDTH):0] cdc_data_in_f;
assign cdc_data_in_f = LAST_FALLING;

wire cdc_fifo_write;
assign cdc_fifo_write = CONF_EN_SYNC ? RISING_SYNC: 1'b0;
wire cdc_fifo_write_f;
assign cdc_fifo_write_f = CONF_EN_FALL_SYNC ? FALLING_SYNC: 1'b0;

wire fifo_full,fifo_write,cdc_fifo_empty;
wire fifo_full_f,fifo_write_f,cdc_fifo_empty_f;
wire wfull,wfull_f;
always@(posedge CLKW) begin
    if(RST_SYNC)
        LOST_DATA_CNT <= 0;
    else if (wfull && cdc_fifo_write && LOST_DATA_CNT != -1)
        LOST_DATA_CNT <= LOST_DATA_CNT +1;
    else if (wfull_f && cdc_fifo_write_f && LOST_DATA_CNT != -1)
            LOST_DATA_CNT <= LOST_DATA_CNT +1;
end

////////////// write fifo
wire [(63+DIV_WIDTH):0] cdc_data_out;
wire [(63+DIV_WIDTH):0] cdc_data_out_f;
wire cdc_fifo_read, cdc_fifo_read_f;
cdc_syncfifo #(
    .DSIZE((64+DIV_WIDTH)), .ASIZE(8)
) cdc_syncfifo (
    .rdata(cdc_data_out),
    .wfull(wfull),
    .rempty(cdc_fifo_empty),
    .wdata(cdc_data_in),
    .winc(cdc_fifo_write), .wclk(CLKW), .wrst(RST_LONG),
    .rinc(cdc_fifo_read), .rclk(BUS_CLK), .rrst(RST_LONG)
);
cdc_syncfifo #(
    .DSIZE((64+DIV_WIDTH)), .ASIZE(8)
) cdc_syncfifo_f (
    .rdata(cdc_data_out_f),
    .wfull(wfull_f),
    .rempty(cdc_fifo_empty_f),
    .wdata(cdc_data_in_f),
    .winc(cdc_fifo_write_f), .wclk(CLKW), .wrst(RST_LONG),
    .rinc(cdc_fifo_read_f), .rclk(BUS_CLK), .rrst(RST_LONG)
);
 
reg [1:0] byte2_cnt, byte2_cnt_prev;
reg [1:0] byte2_cnt_f, byte2_cnt_prev_f;
always@(posedge BUS_CLK) begin
    byte2_cnt_prev <= byte2_cnt;
    byte2_cnt_prev_f <= byte2_cnt_f;
end
assign cdc_fifo_read = (byte2_cnt_prev==0 & byte2_cnt!=0);
assign cdc_fifo_read_f = (byte2_cnt_prev_f==0 & byte2_cnt_f!=0);
assign fifo_write = byte2_cnt_prev != 0;
assign fifo_write_f = byte2_cnt_prev_f != 0;

always@(posedge BUS_CLK)
    if(RST)
        byte2_cnt <= 0;
    else if(!cdc_fifo_empty && !fifo_full && byte2_cnt == 0 ) 
        byte2_cnt <= 3;
    else if (!fifo_full & byte2_cnt != 0)
        byte2_cnt <= byte2_cnt - 1;

always@(posedge BUS_CLK)
    if(RST)
        byte2_cnt_f <= 0;
    else if(!cdc_fifo_empty_f && !fifo_full_f && byte2_cnt_f == 0 ) 
        byte2_cnt_f <= 3;
    else if (!fifo_full_f & byte2_cnt_f != 0)
        byte2_cnt_f <= byte2_cnt_f - 1;

reg [(63+DIV_WIDTH):0] data_buf;
reg [(63+DIV_WIDTH):0] data_buf_f;
always@(posedge BUS_CLK)
    if(cdc_fifo_read)
        data_buf <= cdc_data_out;
always@(posedge BUS_CLK)
    if(cdc_fifo_read_f)
        data_buf_f <= cdc_data_out_f;

wire [25:0] fifo_write_data_byte [3:0];
assign fifo_write_data_byte[0]={2'b01,data_buf[23:0]};
assign fifo_write_data_byte[1]={2'b10,data_buf[47:24]};
assign fifo_write_data_byte[2]={2'b11,data_buf[71:48]};
assign fifo_write_data_byte[3]=26'b0;
wire [25:0] fifo_data_in;
assign fifo_data_in = fifo_write_data_byte[byte2_cnt];
wire [25:0] fifo_data_out;
gerneric_fifo #(
    .DATA_SIZE(26), .DEPTH(1024)
)  fifo (
 .clk(BUS_CLK), .reset(RST_LONG | BUS_RST), 
    .write(fifo_write),
    .read(FIFO_READ), 
    .data_in(fifo_data_in), 
    .full(fifo_full), 
    .empty(FIFO_EMPTY), 
    .data_out(fifo_data_out), .size()
);
wire [25:0] fifo_write_data_byte_f [3:0];
assign fifo_write_data_byte_f[0]={2'b01,data_buf_f[23:0]};
assign fifo_write_data_byte_f[1]={2'b10,data_buf_f[47:24]};
assign fifo_write_data_byte_f[2]={2'b11,data_buf_f[71:48]};
assign fifo_write_data_byte_f[3] = 26'b0;
wire [25:0] fifo_data_in_f;
assign fifo_data_in_f = fifo_write_data_byte_f[byte2_cnt_f];
wire [25:0] fifo_data_out_f;
gerneric_fifo #(
    .DATA_SIZE(26), .DEPTH(1024)
) fifo_f (
 .clk(BUS_CLK), .reset(RST_LONG | BUS_RST), 
    .write(fifo_write_f),
    .read(FIFO_READ_FALL), 
    .data_in(fifo_data_in_f), 
    .full(fifo_full_f), 
    .empty(FIFO_EMPTY_FALL), 
    .data_out(fifo_data_out_f), .size() 
);

assign FIFO_DATA[31:0] = {IDENTIFIER, 2'b00 ,fifo_data_out[25:0]};
assign FIFO_DATA_FALL[31:0] = {IDENTIFIER, 2'b01, fifo_data_out_f[25:0]};

endmodule
