`timescale 1ns / 1ps

`define CODE_FOR_GECCO

/// SiTCP
`default_nettype wire
`include "WRAP_SiTCP_GMII_XC7A_32K.V"
`include "SiTCP_XC7A_32K_BBT_V110.V"
`include "TIMER.v"
`default_nettype none
/// basil
`include "utils/rgmii_io.v"
`include "utils/rbcp_to_bus.v"
`include "utils/cdc_syncfifo.v"
`include "utils/fifo_32_to_8.v"
`include "utils/generic_fifo.v"
/// modules for this chip
`include "telepix_core.v"
`include "telepix_rx/telepix_rx.v"
`include "telepix_rx/telepix_rx_core.v"
`include "telepix_rx/decode_8b10b.v"
`include "telepix_rx/rec_sync.v"
/// basil modlues used in the core
`include "rrp_arbiter/rrp_arbiter.v"
`include "spi/spi_core.v"
`include "spi/spi.v"
`include "spi/blk_mem_gen_8_to_1_2k.v"
`include "utils/bus_to_ip.v"
`include "utils/cdc_pulse_sync.v"
`include "utils/CG_MOD_pos.v"
`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"
`include "utils/3_stage_synchronizer.v"
`include "utils/clock_divider.v"
`include "gpio/gpio_core.v"
`include "gpio/gpio.v"
`include "utils/cdc_reset_sync.v"
`include "utils/flag_domain_crossing.v"
`include "two_phase_spi/two_phase_spi_core.v"
`include "two_phase_spi/two_phase_spi.v"
`include "timestamp_div/timestamp_div_core.v"
`include "timestamp_div/timestamp_div.v"
`include "utils/ddr_des.v"
`include "pulse_gen_div/pulse_gen_div_core.v"
`include "pulse_gen_div/pulse_gen_div.v"

module telepix_nexys(
    input wire cpu_resetn,
    input wire sysclk,
// Ethernet
    output wire [3:0] phy_txd,
    output wire phy_tx_ctl,
    output wire phy_tx_clk,
    input wire [3:0] phy_rxd,
    input wire phy_rx_ctl,
    input wire phy_rx_clk,
    output wire phy_mdc,
    inout wire phy_mdio,
    output wire phy_reset_n,
// Chip
    output wire RstAnalogB,
    output wire CkExt_N,
    output wire CkExt_P,
    output wire CkRef_N,
    output wire CkRef_P,
    output wire SyncRes_N,
    output wire SyncRes_P,
    input wire Data_N,
    input wire Data_P,

    output wire CSIn,
    output wire CCk1,
    output wire CCk2,
    output wire CRb,
    output wire CLd,
    input wire CSOut,
// PCB
   output wire GECCO_SCLK_N,
   output wire GECCO_SCLK_P,
   output wire GECCO_SDI_N,
   output wire GECCO_SDI_P,
   output wire GECCO_SLD_N,
   output wire GECCO_SLD_P,
   output wire INJ_CHOPPER_N,
   output wire INJ_CHOPPER_P,
   output wire INJ_LOOP,
   output wire INJ_PMOD,
   input wire INJ_LOOP_BACK,
   output wire vadj_en,
   output wire [1:0] set_vadj,
//FTDI
    inout wire [7:0] prog_d,
    input wire       prog_rxen,
    input wire       prog_txen,
    output wire      prog_rdn,
    output wire      prog_wrn,
    output wire      prog_siwun,
    output wire      prog_oen,
    input wire       prog_clko,
// debug
    output wire [7:0] LED
);
// -------  PLL for communication with FPGA  ------- //
wire CLK200PLLDLY, CLK200DLY;
wire PLL_FEEDBACK, LOCKED;
wire CLK250PLL, CLK125PLLTX, CLK125PLLTX90, CLK125PLLRX;

IDELAYCTRL IDELAYCTRL_inst (
    .RDY   (         ), // 1-bit Ready output
    .REFCLK( CLK200DLY  ), // 1-beit Reference clock input
    .RST   ( ~LOCKED )  // 1-bit Reset input
);

PLLE2_BASE #(
    .BANDWIDTH("OPTIMIZED"),  // OPTIMIZED, HIGH, LOW
    .CLKFBOUT_MULT(10),       // Multiply value for all CLKOUT, (2-64)
    .CLKFBOUT_PHASE(0.0),     // Phase offset in degrees of CLKFB, (-360.000-360.000).
    .CLKIN1_PERIOD(10.000),   // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
    .DIVCLK_DIVIDE(1),        // Master division value, (1-56)
    .REF_JITTER1(0.0),        // Reference input jitter in UI, (0.000-0.999).
    .STARTUP_WAIT("FALSE"),   // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    
    .CLKOUT0_DIVIDE(7),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT0_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT0_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT1_DIVIDE(4),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT1_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT1_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT2_DIVIDE(8),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT2_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT2_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT3_DIVIDE(8),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT3_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT3_PHASE(90.0),     // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT4_DIVIDE(8),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT4_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT4_PHASE(-5.607),     // Phase offset for CLKOUT0 (-360.000-360.000). //-65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0; TODO(TH) what is this comment means? 

    .CLKOUT5_DIVIDE(5),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT5_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT5_PHASE(0)         // Phase offset for CLKOUT0 (-360.000-360.000).
 ) PLLE2_BASE_inst_comm (
    .CLKOUT0(),
    .CLKOUT1(CLK250PLL),
    .CLKOUT2(CLK125PLLTX),
    .CLKOUT3(CLK125PLLTX90),
    .CLKOUT4(CLK125PLLRX),
    .CLKOUT5(CLK200PLLDLY),
    
    .CLKFBOUT(PLL_FEEDBACK),
    
    .LOCKED(LOCKED),       // 1-bit output: LOCK
    .CLKIN1(sysclk),       // Input 100 MHz clock
    .PWRDWN(0),            // Control Ports
    .RST(!cpu_resetn), 
    .CLKFBIN(PLL_FEEDBACK) // Feedback
 );
wire CLK125TX, CLK125TX90, CLK125RX;
BUFG BUFG_inst_CLK125TX ( .O(CLK125TX), .I(CLK125PLLTX) );
BUFG BUFG_inst_CLK125TX90 ( .O(CLK125TX90), .I(CLK125PLLTX90) );
BUFG BUFG_inst_CLK125RX ( .O(CLK125RX), .I(phy_rx_clk) );
BUFG BUFG_inst_CLK200DLY ( .O(CLK200DLY), .I(CLK200PLLDLY) );

// -------  PLL for clk synthesis  ------- //
(* KEEP = "{TRUE}" *) wire CLK20;
(* KEEP = "{TRUE}" *) wire CLK40;
(* KEEP = "{TRUE}" *) wire CLK200;
(* KEEP = "{TRUE}" *) wire CLK100;
(* KEEP = "{TRUE}" *) wire CLK400;
(* KEEP = "{TRUE}" *) wire BUS_CLK; //100M

wire FEEDBACK2_PLL, FEEDBACK2, LOCKED2;
wire CLK100_PLL, CLK20_PLL, CLK200_PLL, CLK40_PLL, CLK400_PLL, BUS_CLK_PLL;

MMCME2_ADV #(.BANDWIDTH  ("OPTIMIZED"),
    //.CLKOUT4_CASCADE      ("FALSE"),
    .COMPENSATION         ("ZHOLD"),
    .STARTUP_WAIT         ("FALSE"),
    .DIVCLK_DIVIDE        (1),
    .CLKFBOUT_MULT_F      (8.000),
    .CLKFBOUT_PHASE       (0.000),
    .CLKFBOUT_USE_FINE_PS ("FALSE"),
    .CLKOUT0_DIVIDE_F     (8.000),  //100MHz.CLKOUT0             (CLK100_PLL),
    .CLKOUT0_PHASE        (0.000),
    .CLKOUT0_DUTY_CYCLE   (0.500),
    .CLKOUT0_USE_FINE_PS  ("FALSE"),
    .CLKOUT1_DIVIDE       (40),    // 20MHz .CLKOUT1             (CLK20_PLL),
    .CLKOUT1_PHASE        (0.000),
    .CLKOUT1_DUTY_CYCLE   (0.500),
    .CLKOUT1_USE_FINE_PS  ("FALSE"),
    .CLKOUT2_DIVIDE       (4),    //200MHz .CLKOUT2             (CLK200_PLL),
    .CLKOUT2_PHASE        (0.000),
    .CLKOUT2_DUTY_CYCLE   (0.500),
    .CLKOUT2_USE_FINE_PS  ("FALSE"),

    .CLKOUT3_DIVIDE       (20),  //40MHz .CLKOUT3             (CLK40_PLL),
    .CLKOUT3_PHASE        (0.000),
    .CLKOUT3_DUTY_CYCLE   (0.500),
    .CLKOUT3_USE_FINE_PS  ("FALSE"),

    .CLKOUT4_DIVIDE       (2),  //400MHz .CLKOUT4             (CLK400_PLL),
    .CLKOUT4_PHASE        (0.000),
    .CLKOUT4_DUTY_CYCLE   (0.500),
    .CLKOUT4_USE_FINE_PS  ("FALSE"),
    .CLKOUT5_DIVIDE       (8),  // .CLKOUT5             (BUS_CLK_PLL),//100M
    .CLKOUT5_PHASE        (0.000),
    .CLKOUT5_DUTY_CYCLE   (0.500),
    .CLKOUT5_USE_FINE_PS  ("FALSE"),
    .CLKIN1_PERIOD        (10.000)
) mmcm_adv_inst (  // Output clocks
    .CLKFBOUT            (FEEDBACK2_PLL),
    .CLKFBOUTB           (),
    .CLKOUT0             (CLK100_PLL),
    .CLKOUT0B            (),
    .CLKOUT1             (CLK20_PLL),
    .CLKOUT1B            (),
    .CLKOUT2             (CLK200_PLL),
    .CLKOUT2B            (),
    .CLKOUT3             (CLK40_PLL),
    .CLKOUT3B            (),
    .CLKOUT4             (CLK400_PLL),
    .CLKOUT5             (BUS_CLK_PLL),//100M
    .CLKOUT6             (),
     // Input clock control
    .CLKFBIN             (FEEDBACK2),
    .CLKIN1              (sysclk),
    .CLKIN2              (1'b0),
     // Tied to always select the primary input clock
    .CLKINSEL            (1'b1),
    // Ports for dynamic reconfiguration
    .DADDR               (7'h0),
    .DCLK                (1'b0),
    .DEN                 (1'b0),
    .DI                  (16'h0),
    .DO                  (),
    .DRDY                (),
    .DWE                 (1'b0),
    // Ports for dynamic phase shift
    .PSCLK               (1'b0),
    .PSEN                (1'b0),
    .PSINCDEC            (1'b0),
    .PSDONE              (),
    // Other control and status signals
    .LOCKED              (LOCKED2),
    .CLKINSTOPPED        (),
    .CLKFBSTOPPED        (),
    .PWRDWN              (1'b0),
    .RST                 (!cpu_resetn)
);

// PLLE2_BASE #(
    // .BANDWIDTH("OPTIMIZED"),  // OPTIMIZED, HIGH, LOW
    // .CLKFBOUT_MULT(48),       // Multiply value for all CLKOUT, (2-64)
    // .CLKFBOUT_PHASE(0.0),     // Phase offset in degrees of CLKFB, (-360.000-360.000).
    // .CLKIN1_PERIOD(10.000),      // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
    // .DIVCLK_DIVIDE(5),        // Master division value, (1-56)
    // .REF_JITTER1(0.0),        // Reference input jitter in UI, (0.000-0.999).
    // .STARTUP_WAIT("FALSE"),     // Delay DONE until PLL Locks, ("TRUE"/"FALSE")

    // .CLKOUT0_DIVIDE(120),     // Divide amount for CLKOUT0 (1-128)
    // .CLKOUT0_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    // .CLKOUT0_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    // .CLKOUT1_DIVIDE(60),     // Divide amount for CLKOUT0 (1-128)
    // .CLKOUT1_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    // .CLKOUT1_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    // .CLKOUT2_DIVIDE(24),     // Divide amount for CLKOUT0 (1-128)
    // .CLKOUT2_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    // .CLKOUT2_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    // .CLKOUT3_DIVIDE(6),     // Divide amount for CLKOUT0 (1-128)
    // .CLKOUT3_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    // .CLKOUT3_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    // .CLKOUT4_DIVIDE(3),     // Divide amount for CLKOUT0 (1-128)
    // .CLKOUT4_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    // .CLKOUT4_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).
    
    // .CLKOUT5_DIVIDE(7),     // Divide amount for CLKOUT0 (1-128)
    // .CLKOUT5_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    // .CLKOUT5_PHASE(0.0)      // Phase offset for CLKOUT0 (-360.000-360.000).
 // ) PLLE2_BASE_inst_clk (
    // .CLKOUT0(CLK8_PLL),
    // .CLKOUT1(CLK16_PLL),
    // .CLKOUT2(CLK40_PLL),
    // .CLKOUT3(CLK160_PLL),
    // .CLKOUT4(CLK320_PLL),
    // .CLKOUT5(BUS_CLK_PLL),
    
    // .CLKFBOUT(PLL_FEEDBACK2),
    
    // .LOCKED(LOCKED2),       // 1-bit output: LOCK
    // .CLKIN1(sysclk),        // Input 100 MHz clock
    // .PWRDWN(0),             // Control Ports
    // .RST(!cpu_resetn),
    // .CLKFBIN(PLL_FEEDBACK2) // Feedback
 // );
BUFG BUFG_inst_FEEDBACK2 ( .O(FEEDBACK2), .I(FEEDBACK2_PLL) );
BUFG BUFG_inst_CLK100 ( .O(CLK100), .I(CLK100_PLL) );
BUFG BUFG_inst_CLK20 ( .O(CLK20), .I(CLK20_PLL) );
BUFG BUFG_inst_CLK200 ( .O(CLK200), .I(CLK200_PLL) );
BUFG BUFG_inst_CLK40 ( .O(CLK40), .I(CLK40_PLL) );
BUFG BUFG_inst_CLK400 ( .O(CLK400), .I(CLK400_PLL) );
BUFG BUFG_inst_BUS_CKL ( .O(BUS_CLK), .I(BUS_CLK_PLL) );

// -------  TCP interface  ------- //
wire RST;
assign RST = !cpu_resetn | !LOCKED;
wire   gmii_tx_clk;
wire   gmii_tx_en;
wire  [7:0] gmii_txd;
wire   gmii_tx_er;
wire   gmii_crs;
wire   gmii_col;
wire   gmii_rx_clk;
wire   gmii_rx_dv;
wire  [7:0] gmii_rxd;
wire   gmii_rx_er;
wire   mdio_gem_mdc;
wire   mdio_gem_i;
wire   mdio_gem_o;
wire   mdio_gem_t;
wire   link_status;
wire  [1:0] clock_speed;
wire   duplex_status;
rgmii_io rgmii
(
    .rgmii_txd(phy_txd),
    .rgmii_tx_ctl(phy_tx_ctl),
    .rgmii_txc(phy_tx_clk),

    .rgmii_rxd(phy_rxd),
    .rgmii_rx_ctl(phy_rx_ctl),

    .gmii_txd_int(gmii_txd),      // Internal gmii_txd signal.
    .gmii_tx_en_int(gmii_tx_en),
    .gmii_tx_er_int(gmii_tx_er),
    .gmii_col_int(gmii_col),
    .gmii_crs_int(gmii_crs),
    .gmii_rxd_reg(gmii_rxd),   // RGMII double data rate data valid.
    .gmii_rx_dv_reg(gmii_rx_dv), // gmii_rx_dv_ibuf registered in IOBs.
    .gmii_rx_er_reg(gmii_rx_er), // gmii_rx_er_ibuf registered in IOBs.

    .eth_link_status(link_status),
    .eth_clock_speed(clock_speed),
    .eth_duplex_status(duplex_status),
                                     // FOllowing are generated by DCMs
    .tx_rgmii_clk_int(CLK125TX),     // Internal RGMII transmitter clock.
    .tx_rgmii_clk90_int(CLK125TX90),   // Internal RGMII transmitter clock w/ 90 deg phase
    .rx_rgmii_clk_int(CLK125RX),     // Internal RGMII receiver clock

    .reset(!phy_reset_n)
);

IOBUF i_iobuf_mdio(
    .O(mdio_gem_i),
    .IO(phy_mdio),
    .I(mdio_gem_o),
    .T(mdio_gem_t)
);

wire EEPROM_CS, EEPROM_SK, EEPROM_DI;
wire TCP_CLOSE_REQ;
wire RBCP_ACT, RBCP_WE, RBCP_RE;
wire [7:0] RBCP_WD, RBCP_RD;
wire [31:0] RBCP_ADDR;
wire TCP_RX_WR;
wire [7:0] TCP_RX_DATA;
wire RBCP_ACK;
wire SiTCP_RST;
wire TCP_TX_FULL;
wire TCP_TX_WR;
wire [7:0] TCP_TX_DATA;
    
WRAP_SiTCP_GMII_XC7A_32K sitcp(
    .CLK(BUS_CLK),    // in    : System Clock >129MHz
    .RST(RST),        // in    : System reset
    // Configuration parameters
    .FORCE_DEFAULTn(1'b0),     // in: =0 (it does not make sence, but must be 0)   
    .EXT_IP_ADDR(32'hc0a80a11),    // in    : IP address[31:0] 192.168.10.17  16=telescope, 17=this
    .EXT_TCP_PORT(16'd24)        ,    // in    : TCP port #[15:0]
    .EXT_RBCP_PORT(16'd4660)        ,    // in    : RBCP port #[15:0]
    .PHY_ADDR(5'd3),    // in    : PHY-device MIF address[4:0]
    // EEPROM
    .EEPROM_CS(EEPROM_CS)            ,    // out    : Chip select
    .EEPROM_SK(EEPROM_SK)            ,    // out    : Serial data clock
    .EEPROM_DI(EEPROM_DI)            ,    // out    : Serial write data
    .EEPROM_DO(1'b0),    // in    : Serial read data
    // user data, intialial values are stored in the EEPROM, 0xFFFF_FC3C-3F
    .USR_REG_X3C()            ,    // out    : Stored at 0xFFFF_FF3C
    .USR_REG_X3D()            ,    // out    : Stored at 0xFFFF_FF3D
    .USR_REG_X3E()            ,    // out    : Stored at 0xFFFF_FF3E
    .USR_REG_X3F()            ,    // out    : Stored at 0xFFFF_FF3F
    // MII interface
    .GMII_RSTn(phy_reset_n)   ,    // out    : PHY reset
    .GMII_1000M(1'b1)         ,    // in    : GMII mode (0:MII, 1:GMII)
    // TX 
    .GMII_TX_CLK(CLK125TX)    ,    // in    : Tx clock
    .GMII_TX_EN(gmii_tx_en)   ,    // out    : Tx enable
    .GMII_TXD(gmii_txd)       ,    // out    : Tx data[7:0]
    .GMII_TX_ER(gmii_tx_er)   ,    // out    : TX error
    // RX
    .GMII_RX_CLK(CLK125RX)    ,    // in    : Rx clock
    .GMII_RX_DV(gmii_rx_dv)   ,    // in    : Rx data valid
    .GMII_RXD(gmii_rxd)       ,    // in    : Rx data[7:0]
    .GMII_RX_ER(gmii_rx_er)   ,    // in    : Rx error
    .GMII_CRS(gmii_crs)       ,    // in    : Carrier sense
    .GMII_COL(gmii_col)       ,    // in    : Collision detected
    // Management IF
    .GMII_MDC(phy_mdc)        ,    // out    : Clock for MDIO
    .GMII_MDIO_IN(mdio_gem_i) ,    // in    : Data
    .GMII_MDIO_OUT(mdio_gem_o),    // out    : Data
    .GMII_MDIO_OE(mdio_gem_t) ,    // out    : MDIO output enable
    // User I/F
    .SiTCP_RST(SiTCP_RST)            ,    // out    : Reset for SiTCP and related circuits
    // TCP connection control
    .TCP_OPEN_REQ(1'b0)        ,    // in    : Reserved input, shoud be 0
    .TCP_OPEN_ACK()        ,    // out    : Acknowledge for open (=Socket busy)
    .TCP_ERROR()            ,    // out    : TCP error, its active period is equal to MSL
    .TCP_CLOSE_REQ(TCP_CLOSE_REQ)        ,    // out    : Connection close request
    .TCP_CLOSE_ACK(TCP_CLOSE_REQ)        ,    // in    : Acknowledge for closing
    // FIFO I/F
    .TCP_RX_WC(1'b1)            ,    // in    : Rx FIFO write count[15:0] (Unused bits should be set 1)
    .TCP_RX_WR(TCP_RX_WR)            ,    // out    : Write enable
    .TCP_RX_DATA(TCP_RX_DATA)            ,    // out    : Write data[7:0]
    .TCP_TX_FULL(TCP_TX_FULL)            ,    // out    : Almost full flag
    .TCP_TX_WR(TCP_TX_WR)            ,    // in    : Write enable
    .TCP_TX_DATA(TCP_TX_DATA)            ,    // in    : Write data[7:0]
    // RBCP
    .RBCP_ACT(RBCP_ACT)            ,    // out    : RBCP active
    .RBCP_ADDR(RBCP_ADDR)            ,    // out    : Address[31:0]
    .RBCP_WD(RBCP_WD)                ,    // out    : Data[7:0]
    .RBCP_WE(RBCP_WE)                ,    // out    : Write enable
    .RBCP_RE(RBCP_RE)                ,    // out    : Read enable
    .RBCP_ACK(RBCP_ACK)            ,    // in    : Access acknowledge
    .RBCP_RD(RBCP_RD)                    // in    : Read data[7:0]
);

// -------  basil local bus  ------- //
wire BUS_WR, BUS_RD, BUS_RST;
wire [31:0] BUS_ADD;
wire [7:0] BUS_DATA;
assign BUS_RST = SiTCP_RST;

rbcp_to_bus irbcp_to_bus(
    .BUS_RST(BUS_RST),
    .BUS_CLK(BUS_CLK),

    .RBCP_ACT(RBCP_ACT),
    .RBCP_ADDR(RBCP_ADDR),
    .RBCP_WD(RBCP_WD),
    .RBCP_WE(RBCP_WE),
    .RBCP_RE(RBCP_RE),
    .RBCP_ACK(RBCP_ACK),
    .RBCP_RD(RBCP_RD),

    .BUS_WR(BUS_WR),
    .BUS_RD(BUS_RD),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA)
); 

// -------  MODULES for fast data readout(FIFO) - cdc_fifo is for timing reasons
wire ARB_READY_OUT,ARB_WRITE_OUT;
wire [31:0]ARB_DATA_OUT;
wire FIFO_FULL, FIFO_NEAR_FULL;
wire FIFO_EMPTY;
wire [31:0] cdc_data_out;
wire full_32to8, cdc_fifo_empty;

cdc_syncfifo #(.DSIZE(32), .ASIZE(3)
) cdc_syncfifo_i (
    .rdata(cdc_data_out),
    .wfull(FIFO_FULL),
    .rempty(cdc_fifo_empty),
    .wdata(ARB_DATA_OUT),
    .winc(ARB_WRITE_OUT), .wclk(BUS_CLK), .wrst(BUS_RST),
    .rinc(!full_32to8), .rclk(BUS_CLK), .rrst(BUS_RST)
);
assign ARB_READY_OUT = !FIFO_FULL;

fifo_32_to_8 #(.DEPTH(32*1024)
) i_data_fifo (
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    
    .WRITE(!cdc_fifo_empty),
    .READ(TCP_TX_WR),
    .DATA_IN(cdc_data_out),
    .FULL(full_32to8),
    .EMPTY(FIFO_EMPTY),
    .DATA_OUT(TCP_TX_DATA)
);
assign TCP_TX_WR = !TCP_TX_FULL && !FIFO_EMPTY;

// -------  USER CORE ------- //
// LVDS buffers
wire [6:0] obuf_p;
wire [6:0] obuf_n;
wire [6:0] obuf_i;
wire INJ_CHOPPER, GECCO_SCLK, GECCO_SDI, GECCO_SLD, DATA_CLK;
wire CkExt, CkRef, SyncRes;
assign obuf_i = {INJ_CHOPPER, ~GECCO_SCLK, GECCO_SDI, ~GECCO_SLD, CkExt, CkRef, SyncRes};  //vb_clock and vb_load are connected inverted to the receivers on GECCO board
assign {INJ_CHOPPER_P, GECCO_SCLK_P, GECCO_SDI_P, GECCO_SLD_P, CkExt_P, CkRef_P, SyncRes_P} = obuf_p;
assign {INJ_CHOPPER_N, GECCO_SCLK_N, GECCO_SDI_N, GECCO_SLD_N, CkExt_N, CkRef_N, SyncRes_N} = obuf_n;
genvar i;
generate
    for (i = 0; i < 7; i = i + 1) begin
        OBUFDS #(
            .IOSTANDARD("LVDS_25")
        ) OBUFDS_I (
            .I(obuf_i[i]),
            .O(obuf_p[i]),
            .OB(obuf_n[i])
        );
    end
endgenerate

wire Data;
IBUFDS #(
    .DIFF_TERM  ("FALSE"),// Differential termination
    .IOSTANDARD ("LVDS_25")
) IBUFDS_I (
    .I(Data_P),
    .IB(Data_N),
    .O(Data)
);

telepix_core telepix_core(
//local bus
    .BUS_CLK(BUS_CLK),
    .BUS_DATA(BUS_DATA),
    .BUS_ADD(BUS_ADD[15:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_RST(BUS_RST),
//clocks
    .CLK20(CLK20),  //TODO 600MHz is needed!
    .CLK40(CLK40),
    .CLK100(CLK100),
    .CLK200(CLK200),
    .CLK400(CLK400),
//fifo
    .ARB_READY_OUT(ARB_READY_OUT),
    .ARB_WRITE_OUT(ARB_WRITE_OUT),
    .ARB_DATA_OUT(ARB_DATA_OUT),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(FIFO_NEAR_FULL),
// chip
    .RstAnalogB(RstAnalogB),
    .CkExt(CkExt),
    .CkRef(CkRef),
    .SyncRes(SyncRes),
    .Data(Data),
    .CSIn(CSIn),
    .CCk1(CCk1),
    .CCk2(CCk2),
    .CLd(CLd),
    .CRb(CRb),
    .CSOut(CSOut),
// PCB
    .INJ_CHOPPER(INJ_CHOPPER),
    .INJ_LOOP_BACK(INJ_LOOP_BACK),
    .INJ_LOOP(INJ_LOOP),
    .INJ_PMOD(INJ_PMOD),
    .GECCO_SCLK(GECCO_SCLK),
    .GECCO_SDI(GECCO_SDI), 
    .GECCO_SLD(GECCO_SLD),
    .vadj_en(vadj_en),
    .set_vadj(set_vadj),
    .prog_siwun(prog_siwun),
// LED, trigger, etc
    .LED(LED)
);
assign FIFO_NEAR_FULL = 1'b0;
endmodule
