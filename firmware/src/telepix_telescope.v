`timescale 1ns / 1ps

`define CODE_FOR_MIO3

/// SiTCP
`default_nettype wire
`include "WRAP_SiTCP_GMII_XC7K_32K.V"
`include "SiTCP_XC7K_32K_BBT_V110.V"
`include "TIMER.v"
`default_nettype none
/// basil
`include "utils/rgmii_io.v"
`include "utils/rbcp_to_bus.v"
`include "utils/cdc_syncfifo.v"
`include "utils/fifo_32_to_8.v"
`include "utils/generic_fifo.v"
/// modules for telepix
`include "telepix_core.v"
`include "telepix_rx/telepix_rx.v"
`include "telepix_rx/telepix_rx_core.v"
`include "telepix_rx/rec_sync.v"
`include "telepix_rx/decode_8b10b.v"
/// basil modlues used in the core
`include "rrp_arbiter/rrp_arbiter.v"
`include "spi/spi_core.v"
`include "spi/spi.v"
`include "spi/blk_mem_gen_8_to_1_2k.v"
`include "utils/bus_to_ip.v"
`include "utils/cdc_pulse_sync.v"
`include "utils/CG_MOD_pos.v"
`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"
`include "utils/3_stage_synchronizer.v"
`include "utils/clock_divider.v"
`include "gpio/gpio_core.v"
`include "gpio/gpio.v"
`include "utils/cdc_reset_sync.v"
`include "utils/flag_domain_crossing.v"
`include "two_phase_spi/two_phase_spi_core.v"
`include "two_phase_spi/two_phase_spi.v"
`include "timestamp_div/timestamp_div_core.v"
`include "timestamp_div/timestamp_div.v"
`include "timestamp/timestamp_core.v"
`include "timestamp/timestamp.v"
`include "utils/ddr_des.v"
`include "pulse_gen_div/pulse_gen_div_core.v"
`include "pulse_gen_div/pulse_gen_div.v"

module telepix_telescope #(
    parameter NCHIP = 1
) (
    input wire cpu_resetn,
    input wire sysclk_n,
    input wire sysclk_p,
// Ethernet
    output wire [3:0] eth_txd,
    output wire ETH_TX_EN,
    output wire eth_txck,
    input wire [3:0] eth_rxd,
    input wire eth_rxctl,
    input wire eth_rxck,
    output wire eth_mdc,
    inout wire eth_mdio,
    output wire ETH_PHYRST_N,
    input wire eth_int_b,
    input wire eth_pme_b,
// Chip
    output wire [NCHIP-1:0] CkExt_P,
    output wire [NCHIP-1:0] CkExt_N,
    input wire [NCHIP-1:0] DataOut_N,
    input wire [NCHIP-1:0] DataOut_P,
    output wire [NCHIP-1:0] SyncRes_P,
    output wire [NCHIP-1:0] SyncRes_N,
    output wire [NCHIP-1:0] CkRef_N,
    output wire [NCHIP-1:0] CkRef_P,
    output wire DigInj,
    output wire Res_n,
    input wire [NCHIP-1:0] HBOut_P,
    input wire [NCHIP-1:0] HBOut_N,

    output wire CSIn,
    output wire CCk1,
    output wire CCk2,
    output wire [NCHIP-1:0] CLd,
// PCB
    output wire INJ_PMOD,
    output wire INJ_LOOP,
    input wire INJ_LOOP_BACK,
// debug
    output wire [7:0] LED
);
// -------  PLL for communication with FPGA  ------- //
wire sysclk;
IBUFGDS clk_inst (
    .O(sysclk),
    .I(sysclk_p),
    .IB(sysclk_n)
);

wire CLK200LOCKPLL, CLK200LOCK;
IDELAYCTRL IDELAYCTRL_inst (
    .RDY   (         ), // 1-bit Ready output
    .REFCLK( CLK200LOCK  ), // 1-beit Reference clock input
    .RST   ( ~LOCKED )  // 1-bit Reset input
);

wire CLK125PLLTX, CLK125PLLTX90; //, CLK125PLLRX, CLK250PLL;
wire PLL_FEEDBACK, LOCKED;
(* KEEP = "{TRUE}" *) wire BUS_CLK;
wire BUS_CLK_PLL;

PLLE2_BASE #(
    .BANDWIDTH("OPTIMIZED"),  // OPTIMIZED, HIGH, LOW
    .CLKFBOUT_MULT(5),       // Multiply value for all CLKOUT, (2-64)
    .CLKFBOUT_PHASE(0.0),     // Phase offset in degrees of CLKFB, (-360.000-360.000).
    .CLKIN1_PERIOD(5.000),   // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
    .DIVCLK_DIVIDE(1),        // Master division value, (1-56)
    .REF_JITTER1(0.0),        // Reference input jitter in UI, (0.000-0.999).
    .STARTUP_WAIT("FALSE"),   // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    
    .CLKOUT0_DIVIDE(7),       // Divide amount for CLKOUT0 (1-128)  BUS_CLK > 130MHz
    .CLKOUT0_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT0_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT1_DIVIDE(4),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT1_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT1_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT2_DIVIDE(8),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT2_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT2_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT3_DIVIDE(8),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT3_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT3_PHASE(90.0),     // Phase offset for CLKOUT0 (-360.000-360.000).

//    .CLKOUT4_DIVIDE(8),       // Divide amount for CLKOUT0 (1-128)
//    .CLKOUT4_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
//    .CLKOUT4_PHASE(-5.60),     // Phase offset for CLKOUT0 (-360.000-360.000).  // resolution is 45°/[CLKOUTn_DIVIDE]  -65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0; TODO(TH) what is this comment means? 

    .CLKOUT4_DIVIDE(5),       // Divide amount for CLKOUT0 (1-128)
    .CLKOUT4_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT4_PHASE(0),         // Phase offset for CLKOUT0 (-360.000-360.000).
      
    .CLKOUT5_DIVIDE(10),       // Divide amount for CLKOUT5 (1-128)
    .CLKOUT5_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT5_PHASE(90.0)      // Phase offset for CLKOUT0 (-360.000-360.000).  
    
 ) PLLE2_BASE_inst_comm (
    .CLKOUT0(BUS_CLK_PLL),  //  MHz BUS_CLK_PLL
    //.CLKOUT1(CLK250PLL),
    .CLKOUT1(),
    .CLKOUT2(CLK125PLLTX),
    .CLKOUT3(CLK125PLLTX90),
    .CLKOUT4(CLK200LOCKPLL),
    .CLKOUT5(),
    
    .CLKFBOUT(PLL_FEEDBACK),
    
    .LOCKED(LOCKED),       // 1-bit output: LOCK
    .CLKIN1(sysclk),       // Input 100 MHz clock
    .PWRDWN(0),            // Control Ports
    .RST(!cpu_resetn), 
    .CLKFBIN(PLL_FEEDBACK) // Feedback
 );
wire CLK125TX, CLK125TX90, CLK125RX;
BUFG BUFG_inst_CLK125TX ( .O(CLK125TX), .I(CLK125PLLTX) );
BUFG BUFG_inst_CLK125TX90 ( .O(CLK125TX90), .I(CLK125PLLTX90) );
BUFG BUFG_inst_CLK125RX ( .O(CLK125RX), .I(eth_rxck) );
BUFG BUFG_inst_CLK200LOCK ( .O(CLK200LOCK), .I(CLK200LOCKPLL) );
BUFG BUFG_inst_BUS_CKL ( .O(BUS_CLK), .I(BUS_CLK_PLL) );

// -------  Clock for user modules  ------- //
(* KEEP = "{TRUE}" *) wire CLK20;
(* KEEP = "{TRUE}" *) wire CLK40;
(* KEEP = "{TRUE}" *) wire CLK100;
(* KEEP = "{TRUE}" *) wire CLK200;
(* KEEP = "{TRUE}" *) wire CLK400;

wire FEEDBACK2_PLL, FEEDBACK2, LOCKED2;
wire CLK20_PLL, CLK40_PLL, CLK100_PLL, CLK200_PLL, CLK400_PLL;

MMCME2_ADV #(.BANDWIDTH            ("OPTIMIZED"),
    .CLKOUT4_CASCADE      ("FALSE"),
    .COMPENSATION         ("ZHOLD"),
    .STARTUP_WAIT         ("FALSE"),
    .DIVCLK_DIVIDE        (2),
    .CLKFBOUT_MULT_F      (8.000), 
    .CLKFBOUT_PHASE       (0.000),
    .CLKFBOUT_USE_FINE_PS ("FALSE"),
   
    .CLKOUT0_DIVIDE_F     (2),    //400
    .CLKOUT0_PHASE        (0.000),
    .CLKOUT0_DUTY_CYCLE   (0.500),
    .CLKOUT0_USE_FINE_PS  ("FALSE"),
    
    .CLKOUT1_DIVIDE       (4),    //200
    .CLKOUT1_PHASE        (0.000),
    .CLKOUT1_DUTY_CYCLE   (0.500),
    .CLKOUT1_USE_FINE_PS  ("FALSE"),
    
    .CLKOUT2_DIVIDE       (8),   //100
    .CLKOUT2_PHASE        (0.000),
    .CLKOUT2_DUTY_CYCLE   (0.500),
    .CLKOUT2_USE_FINE_PS  ("FALSE"),
  
    .CLKOUT3_DIVIDE       (20),  //40
    .CLKOUT3_PHASE        (0.000),
    .CLKOUT3_DUTY_CYCLE   (0.500),
    .CLKOUT3_USE_FINE_PS  ("FALSE"),
    
    .CLKOUT4_DIVIDE       (40),  //20
    .CLKOUT4_PHASE        (0.000),
    .CLKOUT4_DUTY_CYCLE   (0.500),
    .CLKOUT4_USE_FINE_PS  ("FALSE"),
    
    // .CLKOUT5_DIVIDE       (4),  //200
    // .CLKOUT5_PHASE        (0.000),  
    // .CLKOUT5_DUTY_CYCLE   (0.500),
    // .CLKOUT5_USE_FINE_PS  ("FALSE"),   
    
    // .CLKOUT6_DIVIDE       (128),  //10
    // .CLKOUT6_PHASE        (0.000),
    // .CLKOUT6_DUTY_CYCLE   (0.500),
    // .CLKOUT6_USE_FINE_PS  ("FALSE"),  
     
    .CLKIN1_PERIOD        (5.000)
) MMCME2_ADV_i (
    .CLKFBOUT            (FEEDBACK2_PLL),
    .CLKOUT0             (CLK400_PLL),
    .CLKOUT0B            (),
    .CLKOUT1             (CLK200_PLL),
    .CLKOUT1B            (),
    .CLKOUT2             (CLK100_PLL),
    .CLKOUT2B            (),
    .CLKOUT3             (CLK40_PLL),
    .CLKOUT3B            (),
    .CLKOUT4             (CLK20_PLL),
    .CLKOUT5             (),
    .CLKOUT6             (),
     
    .CLKFBIN             (FEEDBACK2_PLL),  // Input clock control
    .CLKIN1              (sysclk),
    .CLKIN2              (1'b0),   
    .CLKINSEL            (1'b1),  // Tied to always select the primary input clock  
    .DADDR               (7'h0),  // Ports for dynamic reconfiguration
    .DCLK                (1'b0),
    .DEN                 (1'b0),
    .DI                  (16'h0),
    .DO                  (),
    .DRDY                (),
    .DWE                 (1'b0),
    .PSCLK               (1'b0),   // Ports for dynamic phase shift
    .PSEN                (1'b0),
    .PSINCDEC            (1'b0),
    .PSDONE              (),
    .LOCKED              (LOCKED2),  // Other control and status signals
    .CLKINSTOPPED        (),
    .CLKFBSTOPPED        (),
    .PWRDWN              (1'b0),
    .RST                 (!cpu_resetn)
);

BUFG BUFG_inst_FEEDBACK2 ( .O(FEEDBACK2), .I(FEEDBACK2_PLL) );
BUFG BUFG_inst_CLK20 ( .O(CLK20), .I(CLK20_PLL) );
BUFG BUFG_inst_CLK40 ( .O(CLK40), .I(CLK40_PLL) );
BUFG BUFG_inst_CLK400 ( .O(CLK400), .I(CLK400_PLL) );
BUFG BUFG_inst_CLK100 ( .O(CLK100), .I(CLK100_PLL) );
BUFG BUFG_inst_CLK200 ( .O(CLK200), .I(CLK200_PLL) );

// -------  TCP interface  ------- //
wire RST;
assign RST = !cpu_resetn | !LOCKED | !LOCKED2;

wire   gmii_tx_clk;
wire   gmii_tx_en;
wire  [7:0] gmii_txd;
wire   gmii_tx_er;
wire   gmii_crs;
wire   gmii_col;
wire   gmii_rx_clk;
wire   gmii_rx_dv;
wire  [7:0] gmii_rxd;
wire   gmii_rx_er;
wire   mdio_gem_mdc;
wire   mdio_gem_i;
wire   mdio_gem_o;
wire   mdio_gem_t;
wire   link_status;
wire  [1:0] clock_speed;
wire   duplex_status;
rgmii_io rgmii
(
    .rgmii_txd(eth_txd),
    .rgmii_tx_ctl(ETH_TX_EN),
    .rgmii_txc(eth_txck),

    .rgmii_rxd(eth_rxd),
    .rgmii_rx_ctl(eth_rxctl),

    .gmii_txd_int(gmii_txd),      // Internal gmii_txd signal.
    .gmii_tx_en_int(gmii_tx_en),
    .gmii_tx_er_int(gmii_tx_er),
    .gmii_col_int(gmii_col),
    .gmii_crs_int(gmii_crs),
    .gmii_rxd_reg(gmii_rxd),   // RGMII double data rate data valid.
    .gmii_rx_dv_reg(gmii_rx_dv), // gmii_rx_dv_ibuf registered in IOBs.
    .gmii_rx_er_reg(gmii_rx_er), // gmii_rx_er_ibuf registered in IOBs.

    .eth_link_status(link_status),
    .eth_clock_speed(clock_speed),
    .eth_duplex_status(duplex_status),

                              // FOllowing are generated by DCMs
    .tx_rgmii_clk_int(CLK125TX),     // Internal RGMII transmitter clock.
    .tx_rgmii_clk90_int(CLK125TX90),   // Internal RGMII transmitter clock w/ 90 deg phase
    .rx_rgmii_clk_int(CLK125RX),     // Internal RGMII receiver clock

    .reset(!ETH_PHYRST_N)
);

IOBUF i_iobuf_mdio(
    .O(mdio_gem_i),
    .IO(eth_mdio),
    .I(mdio_gem_o),
    .T(mdio_gem_t)
);

wire EEPROM_CS, EEPROM_SK, EEPROM_DI;
wire TCP_CLOSE_REQ;
wire RBCP_ACT, RBCP_WE, RBCP_RE;
wire [7:0] RBCP_WD, RBCP_RD;
wire [31:0] RBCP_ADDR;
wire TCP_RX_WR;
wire [7:0] TCP_RX_DATA;
wire RBCP_ACK;
wire SiTCP_RST;
wire TCP_TX_FULL;
wire TCP_TX_WR;
wire [7:0] TCP_TX_DATA;
    
WRAP_SiTCP_GMII_XC7K_32K sitcp(
    .CLK(BUS_CLK),    // in    : System Clock >129MHz
    .RST(RST),        // in    : System reset
    // Configuration parameters
    .FORCE_DEFAULTn(1'b0),     // in: =0 (it does not make sence, but must be 0)   
    .EXT_IP_ADDR(32'hc0a80a10),    // in    : IP address[31:0] 192.168.10.17  16=telescope, 17=this
    .EXT_TCP_PORT(16'd24)        ,    // in    : TCP port #[15:0]
    .EXT_RBCP_PORT(16'd4660)        ,    // in    : RBCP port #[15:0]
    .PHY_ADDR(5'd1),    // in    : PHY-device MIF address[4:0]
    // EEPROM
    .EEPROM_CS(EEPROM_CS)            ,    // out    : Chip select
    .EEPROM_SK(EEPROM_SK)            ,    // out    : Serial data clock
    .EEPROM_DI(EEPROM_DI)            ,    // out    : Serial write data
    .EEPROM_DO(1'b0),                     // in    : Serial read data
    // user data, intialial values are stored in the EEPROM, 0xFFFF_FC3C-3F
    .USR_REG_X3C()            ,    // out    : Stored at 0xFFFF_FF3C
    .USR_REG_X3D()            ,    // out    : Stored at 0xFFFF_FF3D
    .USR_REG_X3E()            ,    // out    : Stored at 0xFFFF_FF3E
    .USR_REG_X3F()            ,    // out    : Stored at 0xFFFF_FF3F
    // MII interface
    .GMII_RSTn(ETH_PHYRST_N)  ,    // out    : PHY reset
    .GMII_1000M(1'b1)         ,    // in    : GMII mode (0:MII, 1:GMII)
    // TX 
    .GMII_TX_CLK(CLK125TX)    ,    // in    : Tx clock
    .GMII_TX_EN(gmii_tx_en)   ,    // out    : Tx enable
    .GMII_TXD(gmii_txd)       ,    // out    : Tx data[7:0]
    .GMII_TX_ER(gmii_tx_er)   ,    // out    : TX error
    // RX
    .GMII_RX_CLK(CLK125RX)    ,    // in    : Rx clock
    .GMII_RX_DV(gmii_rx_dv)   ,    // in    : Rx data valid
    .GMII_RXD(gmii_rxd)       ,    // in    : Rx data[7:0]
    .GMII_RX_ER(gmii_rx_er)   ,    // in    : Rx error
    .GMII_CRS(gmii_crs)       ,    // in    : Carrier sense
    .GMII_COL(gmii_col)       ,    // in    : Collision detected
    // Management IF
    .GMII_MDC(eth_mdc)        ,    // out    : Clock for MDIO
    .GMII_MDIO_IN(mdio_gem_i) ,    // in    : Data
    .GMII_MDIO_OUT(mdio_gem_o),    // out    : Data
    .GMII_MDIO_OE(mdio_gem_t) ,    // out    : MDIO output enable
    // User I/F
    .SiTCP_RST(SiTCP_RST)            ,    // out    : Reset for SiTCP and related circuits
    // TCP connection control
    .TCP_OPEN_REQ(1'b0)        ,    // in    : Reserved input, shoud be 0
    .TCP_OPEN_ACK()        ,    // out    : Acknowledge for open (=Socket busy)
    .TCP_ERROR()            ,    // out    : TCP error, its active period is equal to MSL
    .TCP_CLOSE_REQ(TCP_CLOSE_REQ)        ,    // out    : Connection close request
    .TCP_CLOSE_ACK(TCP_CLOSE_REQ)        ,    // in    : Acknowledge for closing
    // FIFO I/F
    .TCP_RX_WC(1'b1)            ,    // in    : Rx FIFO write count[15:0] (Unused bits should be set 1)
    .TCP_RX_WR(TCP_RX_WR)            ,    // out    : Write enable
    .TCP_RX_DATA(TCP_RX_DATA)            ,    // out    : Write data[7:0]
    .TCP_TX_FULL(TCP_TX_FULL)            ,    // out    : Almost full flag
    .TCP_TX_WR(TCP_TX_WR)            ,    // in    : Write enable
    .TCP_TX_DATA(TCP_TX_DATA)            ,    // in    : Write data[7:0]
    // RBCP
    .RBCP_ACT(RBCP_ACT)            ,    // out    : RBCP active
    .RBCP_ADDR(RBCP_ADDR)            ,    // out    : Address[31:0]
    .RBCP_WD(RBCP_WD)                ,    // out    : Data[7:0]
    .RBCP_WE(RBCP_WE)                ,    // out    : Write enable
    .RBCP_RE(RBCP_RE)                ,    // out    : Read enable
    .RBCP_ACK(RBCP_ACK)            ,    // in    : Access acknowledge
    .RBCP_RD(RBCP_RD)                    // in    : Read data[7:0]
);

// -------  basil local bus  ------- //
wire BUS_WR, BUS_RD, BUS_RST;
wire [31:0] BUS_ADD;
wire [7:0] BUS_DATA;
assign BUS_RST = SiTCP_RST;

rbcp_to_bus irbcp_to_bus(
    .BUS_RST(BUS_RST),
    .BUS_CLK(BUS_CLK),

    .RBCP_ACT(RBCP_ACT),
    .RBCP_ADDR(RBCP_ADDR),
    .RBCP_WD(RBCP_WD),
    .RBCP_WE(RBCP_WE),
    .RBCP_RE(RBCP_RE),
    .RBCP_ACK(RBCP_ACK),
    .RBCP_RD(RBCP_RD),

    .BUS_WR(BUS_WR),
    .BUS_RD(BUS_RD),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA)
); 

// -------  MODULES for fast data readout(FIFO) - cdc_fifo is for timing reasons
wire ARB_READY_OUT,ARB_WRITE_OUT;
wire [31:0]ARB_DATA_OUT;
wire FIFO_FULL,FIFO_NEAR_FULL;
wire FIFO_EMPTY;
wire [31:0] cdc_data_out;
wire full_32to8, cdc_fifo_empty;

cdc_syncfifo #(.DSIZE(32), .ASIZE(3)
) cdc_syncfifo_i (
    .rdata(cdc_data_out),
    .wfull(FIFO_FULL),
    .rempty(cdc_fifo_empty),
    .wdata(ARB_DATA_OUT),
    .winc(ARB_WRITE_OUT), .wclk(BUS_CLK), .wrst(BUS_RST),
    .rinc(!full_32to8), .rclk(BUS_CLK), .rrst(BUS_RST)
);
assign ARB_READY_OUT = !FIFO_FULL;

fifo_32_to_8 #(.DEPTH(32*1024)
) i_data_fifo (
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    
    .WRITE(!cdc_fifo_empty),
    .READ(TCP_TX_WR),
    .DATA_IN(cdc_data_out),
    .FULL(full_32to8),
    .EMPTY(FIFO_EMPTY),
    .DATA_OUT(TCP_TX_DATA)
);
assign TCP_TX_WR = !TCP_TX_FULL && !FIFO_EMPTY;

// -------  USER CORE ------- //
// LVDS buffers
wire [3*NCHIP-1:0] obuf_p;
wire [3*NCHIP-1:0] obuf_n;
wire [3*NCHIP-1:0] obuf_i;
wire [NCHIP-1:0] CkExt;
wire [NCHIP-1:0] SyncRes;
wire [NCHIP-1:0] CkRef;
assign obuf_i = {
                CkExt, SyncRes, CkRef
                };  
assign {
       CkExt_P, SyncRes_P, CkRef_P
       } = obuf_p;
assign {
       CkExt_N, SyncRes_N, CkRef_N
       } = obuf_n;
genvar i;
generate
    for (i = 0; i < 3*NCHIP; i = i + 1) begin
        OBUFDS #(
            .IOSTANDARD("LVDS_25")
        ) OBUFDS_I (
            .I(obuf_i[i]),
            .O(obuf_p[i]),
            .OB(obuf_n[i])
        );
    end
endgenerate

wire [NCHIP-1:0] DataOut;
wire [NCHIP-1:0] HBOut;
IBUFDS_DIFF_OUT #(
    .DIFF_TERM  ("TRUE"),             // Differential termination
    .IOSTANDARD ("LVDS_25")
) IBUFDS_DIFF_OUT_D (
    .I(DataOut_P[0]),
    .IB(DataOut_N[0]),
    .O(DataOut[0])
);
IBUFDS_DIFF_OUT #(
    .DIFF_TERM  ("TRUE"),             // Differential termination
    .IOSTANDARD ("LVDS_25")
) IBUFDS_DIFF_OUT_HB (
    .I(HBOut_P[0]),
    .IB(HBOut_N[0]),
    .O(HBOut[0])
);

telepix_core telepix_core (
//local bus
    .BUS_CLK(BUS_CLK),
    .BUS_DATA(BUS_DATA),
    .BUS_ADD(BUS_ADD),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_RST(BUS_RST),
//clocks
    .CLK20(CLK20),
    .CLK40(CLK40),
    .CLK100(CLK100),
    .CLK200(CLK200),
    .CLK400(CLK400),


//fifo
    .ARB_READY_OUT(ARB_READY_OUT),
    .ARB_WRITE_OUT(ARB_WRITE_OUT),
    .ARB_DATA_OUT(ARB_DATA_OUT),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(FIFO_NEAR_FULL),
// chip
    .TSCkExt(),
    .CkExt(CkExt[0]),
    .SyncRes(SyncRes[0]),
    .CkRef(CkRef[0]),
    .DataOut(DataOut[0]),
    .HBOut(HBOut[0]),
    .DigInj(DigInj),
    .Res_n(Res_n),

    .CSIn(CSIn),
    .CCk1(CCk1),
    .CCk2(CCk2),
    .CRb(),
    .CLd(CLd[0]),
    .CSOut(1'b0),
// PCB
    .INJ_PMOD(INJ_PMOD),
    .INJ_LOOP(INJ_LOOP),
    .INJ_LOOP_BACK(INJ_LOOP_BACK),
// LED, trigger, etc
    .LED(LED)
);
endmodule
