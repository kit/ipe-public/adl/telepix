#!/bin/bash

if [ $# = 0 ]; then
   board="nexys"
else
   board=${1}
fi
echo $board

if [ ${board} = "nexys" ]; then
    source /tools/Xilinx/Vivado/2017.2/settings64.sh
    vivado -mode tcl -source program.tcl
elif [ ${board} = "gui" ]; then
    source /tools/Xilinx/Vivado/2022.2/settings64.sh
    vivado &
else
    source /tools/Xilinx/Vivado/2022.2/settings64.sh
    vivado -mode tcl -source program_${board}.tcl
fi