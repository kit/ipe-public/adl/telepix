# ------------------------------------------------------------
#   Copyright (c) All rights reserved
#   ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
# Usage: vivado -mode tcl -source program.tcl

open_hw
connect_hw_server
open_hw_target {localhost:3121/xilinx_tcf/Digilent/211257161769B}
current_hw_device [get_hw_devices xc7a200t_0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7a200t_0] 0]
set_property PROGRAM.FILE {telepix-nexys.bit} [get_hw_devices xc7a200t_0]
program_hw_devices [get_hw_devices xc7a200t_0]
exit
