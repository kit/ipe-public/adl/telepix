#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#
#  cd firmware/vivado
#  vivado -mode tcl -source program.tcl
#

open_hw_manager
#connect_hw_server -allow_non_jtag
connect_hw_server
#open_hw_target
if {[catch { open_hw_target } errorstring]} {
  puts "!!!!!open_hw_target failed!!!! $errorstring "
  puts "reconnectting.... "
  disconnect_hw_server
  close_hw_manager
  open_hw_manager
  connect_hw_server
  open_hw_target
}
#open_hw_target {localhost:3121/xilinx_tcf/Digilent/200300AFF1BDB}

current_hw_device [get_hw_devices xc7k325t_0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7k325t_0] 0]

set_property PROBES.FILE {} [get_hw_devices xc7k325t_0]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7k325t_0]
set_property PROGRAM.FILE {telepix-telescope.bit} [get_hw_devices xc7k325t_0]

program_hw_devices [get_hw_devices xc7k325t_0]
refresh_hw_device [lindex [get_hw_devices xc7k325t_0] 0]
disconnect_hw_server
close_hw_manager
exit
