#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#


from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer


class telepix_rx(RegisterHardwareLayer):
    '''Telepix receiver controller interface
    '''

    _registers = {'VERSION': {'descr': {'addr': 0, 'size': 8, 'properties': ['ro']}},
                  'RESET': {'descr': {'addr': 0, 'size': 8, 'properties': ['wo']}},
                  'STATUS': {'descr': {'addr': 1, 'size': 4, 'properties': ['ro']}},
                  'LOST_ERR': {'descr': {'addr': 1, 'size': 1, 'offset': 0, 'properties': ['ro']}},
                  'DATA_ERR': {'descr': {'addr': 1, 'size': 1, 'offset': 1, 'properties': ['ro']}},
                  'DECODER_ERR': {'descr': {'addr': 1, 'size': 1, 'offset': 2, 'properties': ['ro']}},
                  'ALIGNED': {'descr': {'addr': 1, 'size': 1, 'offset': 3, 'properties': ['ro']}},
                  'EN': {'descr': {'addr': 1, 'size': 1, 'offset': 4}},
                  'INVERT_RX': {'descr': {'addr': 1, 'size': 1, 'offset': 5}},
                  'CAPTURE_RAW': {'descr': {'addr': 1, 'size': 1, 'offset': 6}},
                  'DUMP_ALL': {'descr': {'addr': 1, 'size': 1, 'offset': 7}},

                  'DATA_DLY': {'descr': {'addr': 2, 'size': 5}},
                  'DATA_DLY_WR': {'descr': {'addr': 2, 'size': 1, 'offset':5}},
                  'SAMPLING_EDGE': {'descr': {'addr': 2, 'size': 1, 'offset': 6}},
                  'EN_TS': {'descr': {'addr': 2, 'size': 1, 'offset': 7}},
                  'FIFO_SIZE': {'descr': {'addr': 3, 'size': 12, 'properties': ['ro']}},
                  'DECODER_ERR_CNT': {'descr': {'addr': 5, 'size': 8, 'properties': ['ro']}},
                  'LOST_COUNT': {'descr': {'addr': 6, 'size': 8, 'properties': ['ro']}},
                  'DATA_ERR_CNT': {'descr': {'addr': 7, 'size': 8, 'properties': ['ro']}},
                  'DATA_DLY_OUT': {'descr': {'addr': 8, 'size': 5, 'properties': ['ro']}},
                  'RAW_DATA0': {'descr': {'addr': 11, 'size': 10, 'properties': ['ro']}},
                  'RAW_DATA1': {'descr': {'addr': 12, 'size': 10, 'offset':2, 'properties': ['ro']}},
                  }

    _require_version = "==1"

    def __init__(self, intf, conf):
        super(telepix_rx, self).__init__(intf, conf)

    def reset(self):
        self.RESET = 0

    def set_en(self, value):
        self.EN = value

    def is_done(self):
        return self.is_ready

    @property
    def is_ready(self):
        return self.READY

    def get_fifo_size(self):
        return self.FIFO_SIZE

    def get_decoder_error_counter(self):
        return self.DECODER_ERROR_COUNTER

    def get_lost_data_counter(self):
        return self.LOST_DATA_COUNTER
