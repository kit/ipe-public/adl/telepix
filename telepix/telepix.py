#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# ASIC and Detector Laboratory, IPE, KIT
# ------------------------------------------------------------
#

import os
import time
import logging
import yaml
import numpy as np
# import tables as tb
from basil.dut import Dut


def mk_fname(ext="data.npy", dirname=None):
    output_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + os.sep + 'output'  # noqa: E501
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(output_dir, prefix)
    if not os.path.exists(dirname):
        os.system("mkdir -p {0:s}".format(dirname))
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_") + ext)


class TelePix(Dut):

    def __init__(self, conf=None, **kwargs):
        # set logger
        self.logger = logging.getLogger()
        logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] (%(threadName)-10s) %(message)s")
        fname = mk_fname(ext="log.log")
        fileHandler = logging.FileHandler(fname)
        fileHandler.setFormatter(logFormatter)
        self.logger.addHandler(fileHandler)

        # define constant and memories
        self.ncols = 120
        self.nrows = 400
        self.PixelConf = {'comp_mask': np.ones([self.ncols, self.nrows], dtype=np.uint8) * 0xFF,
                          'tdac': np.ones([self.ncols, self.nrows], dtype=np.uint8) * 0xFF}

        if conf is None:
            conf = os.path.dirname(os.path.abspath(__file__)) + os.sep + "telepix.yml"
        if isinstance(conf, str):
            with open(conf) as f:
                conf = yaml.safe_load(f)
        super(TelePix, self).__init__(conf=conf)

    def init(self):
        super(TelePix, self).init()
        fw_version = self.get_fw_version()
        logging.info("Firmware version: {0}".format(fw_version))

    def get_fw_version(self):
        return self['intf'].read(0x10000, 1)[0]

    def reconnect(self):
        try:
            self['intf'].close()
        except Exception:
            self.logger.warn('reconnect: port close failed')
        self['intf'].init()

    def reset_chip(self):
        # Rst chip
        self['SW']['Res_n'] = 0
        self['SW'].write()
        self['SW']['Res_n'] = 1
        self['SW'].write()
        # Initialize CONF
        self['CONF'].set_configuration(self['CONF']._init)
        # for conf_name in ['CONF', 'VDAC_CONF', 'DAC_CONF']:
        #    self.set_conf(conf_name)

    def _write_sr(self, conf_name):
            self['SW']['EN_CCk1']=1
            self['SW'].write()
            self[conf_name].set_size(self[conf_name]._conf['size'])
            # Ld
            self[conf_name]['Ld'] = 1
            self[conf_name].write()
            while not self[conf_name].is_ready:
                time.sleep(0.001)
            self.logger.debug('{0}{1}'.format(conf_name, self[conf_name]))

            # self[conf_name]['Ld'] = 0
            # self[conf_name].write()
            # while not self[conf_name].is_ready:
            #     time.sleep(0.001)
            # Ld OFF
            self['SW']['EN_CCk1']=0
            self['SW'].write()
            self['LD_CONF'].set_size(self['LD_CONF']._conf['size'])
            self['LD_CONF'].set_configuration(self['LD_CONF']._init)
            self['LD_CONF'].write()
            while not self['LD_CONF'].is_ready:
               time.sleep(0.001)

    def set_inj(self, inj_n=100, inj_width=10240, inj_delay=10240, inj_phase=-1, ext=False):
        self["inj"].reset()
        self["inj"]["REPEAT"] = inj_n
        self["inj"]["DELAY"] = inj_delay
        self["inj"]["WIDTH"] = inj_width
        self["inj"]["EN"] = ext
        self['gate'].REPEAT = 1
        self['gate'].WIDTH = self['sync'].WIDTH + self['sync'].DELAY
        self['gate'].DELAY = 1

        self.logger.info("set_inj: inj_width={0:d} inj_delay={1:d} inj_phase={2:d} inj_n={3:d} ext={4:d}".format(
            inj_width, inj_delay, inj_phase, inj_n, int(ext)))

    def set_inj_phase(self, inj_phase):
        self["inj"].set_phase(int(inj_phase))
        self.logger.info('set_inj_phase: inj_phase={}'.format(inj_phase))

    def set_inj_amp(self, inj_amp, unit='V'):
        if unit =='V':
            inj_amp = int(inj_amp * 255 / 1.8)
        self["VDAC_CONF"]['vcal']=inj_amp
        self._write_sr('VDAC_CONF')

    def inject(self, wait=False):
        if self["inj"].EN:
            self["gate"].start()
            while (not self["gate"].is_ready) and wait:
                time.sleep(0.001)
        else:
            self["inj"].start()
            while (not self["inj"].is_ready) and wait:
                time.sleep(0.001)

    def set_en_inj(self, pix="none", cols=None, rows=None):
        # make lists of cols and rows
        if rows is None and cols is None:
            if pix is None:
                pix = np.zeros([2, 0], dtype=int)
            elif isinstance(pix, str):  # if none
                pix = np.zeros([2, 0], dtype=int)
            elif len(pix) == 0:
                pix = np.zeros([2, 0], dtype=int)
            elif isinstance(pix[0], int):
                pix = [pix]
            pix = np.array(pix)
            cols = np.unique(pix[0, :])
            rows = np.unique(pix[1, :])
        # prepare conf reg
        if cols is not None:
            for col in np.arange(self.ncols):
                if col in cols:
                    self['ROW_CONF']['ColRev'][self.ncols - col - 1]['EnInjCol'] = 1
                else:
                    self['ROW_CONF']['ColRev'][self.ncols - col - 1]['EnInjCol'] = 0
        if rows is not None:
            for row in np.arange(self.nrows):
                div_row, mod_row = divmod(row, 4)
                if row in rows:
                    self['ROW_CONF']['ColRev'][self.ncols - div_row - 1]['EnRow'][mod_row] = '1'
                else:
                    self['ROW_CONF']['ColRev'][self.ncols - div_row - 1]['EnRow'][mod_row] = '0'
        # write to ROW_CONF
        self._write_sr('ROW_CONF')

        # write to the EnInj switch in CONF
        if cols is None or len(cols) == 0:
            self['CONF']['InjEn'] = 0
        elif rows is None or len(rows) == 0:
            self['CONF']['InjEn'] = 0
        else:
            self['CONF']['InjEn'] = 1
        self._write_sr('CONF')
        # Log
        EnInjCol = 0
        for c in np.arange(self.ncols):
            EnInjCol = (EnInjCol << 1) | self['ROW_CONF']['ColRev'][c]['EnInjCol'].tovalue()
        rows = []
        for r in np.arange(0, self.nrows, 4):  # TODO make a shorter log
            div_r = r // 4
            EnRow = self['ROW_CONF']['ColRev'][self.ncols - div_r - 1]['EnRow'].tovalue()
            for i in range(4):
                if EnRow & (0x1 << i):
                    rows.append(div_r + i)
        self.logger.info('set_inj_en_colrow:InjEn={0} cols={1:030x}, rows={2}'.format(
            self['CONF']['InjEn'].tovalue(),
            EnInjCol,
            rows))

    def set_en_ampout(self, cols='none'):
        # prepare SR values
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):  # if 'none', disable all
            cols = []
        for col in range(self.ncols):
            if col in cols:
                self['ROW_CONF']['ColRev'][self.ncols - 1 - col]['EnSFout'] = '1'
            else:
                self['ROW_CONF']['ColRev'][self.ncols - 1 - col]['EnSFout'] = '0'
        # write to ROW_CONF
        self._write_sr('ROW_CONF')
        # log
        EnSFout = 0
        for c in range(0, self.ncols):
            EnSFout = (EnSFout << 1) | self['ROW_CONF']['ColRev'][c]['EnSFout'].tovalue()
        self.logger.info('set_en_ampout: EnSFout=0x{0:030x}'.format(EnSFout))

    def set_en_hitbus(self, cols='none'):
        # prepare SR values
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):  # if 'none', disable all
            cols = []
        for col in range(self.ncols):
            if col in cols:
                self['ROW_CONF']['ColRev'][self.ncols - 1 - col]['EnHB'] = '1'
            else:
                self['ROW_CONF']['ColRev'][self.ncols - 1 - col]['EnHB'] = '0'
        # write to ROW_CONF
        self._write_sr('ROW_CONF')
        # log
        EnHB = 0
        for c in range(0, self.ncols):
            EnHB = (EnHB << 1) | self['ROW_CONF']['ColRev'][c]['EnHB'].tovalue()
        self.logger.info('set_en_hb: EnHB=0x{0:030x}'.format(EnHB))

    def set_ampout_hb_output(self, IPFoll=30):
        if isinstance(IPFoll, bool):
            if IPFoll:
                IPFoll = 30
            else:
                IPFoll = 0
        # set values
        self['CONF']['IPFoll'] = IPFoll
        # shift in
        self._write_sr('CONF')

    def set_conf(self, **kwargs):
        # set values
        flg = {'CONF': 0, 'VDAC_CONF': 0, 'DAC_CONF': 0}
        check = 0
        for k in kwargs:
            if k == 'init':
                for conf_name in flg:
                    self[conf_name].set_configuration(self[conf_name]._init)
                    flg[conf_name] = 1
            else:
                for conf_name in flg:
                    if k in self[conf_name]._fields:
                        self[conf_name][k] = kwargs[k]
                        flg[conf_name] = 1
                    else:
                        check = check + 1
            if check == 3:
                self.logger.error("set_conf: arg error {}".format(k))
        # write values
        for conf_name in flg:
            if flg[conf_name] == 1:
                self._write_sr(conf_name)
                self.logger.info("set_conf: {0} ".format(self[conf_name]))

    def set_comp(self, comp_mask):  # TODO make private functions (set_comp, init_PixelConf, set_tdac)
        if isinstance(comp_mask, str):
            if comp_mask == 'all':
                comp_mask = np.ones([self.ncols, self.nrows], dtype='u1')
            elif comp_mask == 'none':
                comp_mask = np.zeors([self.ncols, self.nrows], dtype='u1')

        rows = np.unique(np.argwhere(comp_mask != self.PixelConf['comp_mask'])[:, 1])
        # disable Inj, enable RAMWr
        tmp_InjEn = self['CONF']['InjEn'].tovalue()
        self['CONF']['InjEn'] = 0
        self['CONF']['RAMWrEnable'] = 1
        self['CONF']['PCH'] = 0
        self._write_sr('CONF')

        # save Inj rows
        tmp_EnRow = []
        for c in range(self.ncols):
            tmp_EnRow.append(self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'].tovalue())

        for r in rows:
            # set tdac value
            for c in range(self.ncols):
                self['TDAC_CONF']['Col'][c] = comp_mask[c, r]
            self._write_sr('TDAC_CONF')
            # select row
            for c in range(self.ncols):
                self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'] = 0
            div_r, mod_r = divmod(r, 4)
            self['ROW_CONF']['ColRev'][self.ncols - div_r - 1]['EnRow'][mod_r] = '1'
            self._write_sr('ROW_CONF')
            # write RAM
            self['CONF']['WrRAM_Comp'] = 1
            self._write_sr('CONF')

            # log and update memory
            self.PixelConf['comp_mask'][:, r] = comp_mask[:, r]
            self.logger.info('set_comp: row={0},mask=0x{1}'.format(r, self['TDAC_CONF']['Col'].to01()))

            # set WrRAM to 0
            self['CONF']['WrRAM_Comp'] = 0
            self._write_sr('CONF')

        # restore En Inj
        self['CONF']['InjEn'] = tmp_InjEn
        self['CONF']['RAMWrEnable'] = 0
        self._write_sr('CONF')

        # restore EnRow
        for c in range(self.ncols):
            self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'] = tmp_EnRow[c]
        self._write_sr('ROW_CONF')

    def set_tdac(self, tdac):
        if isinstance(tdac, int):
            tdac = np.ones([self.ncols, self.nrows], dtype='u1') * tdac

        # save Inj rows
        tmp_EnRow = []
        for c in range(self.ncols):
            tmp_EnRow.append(self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'].tovalue())
        tmp_InjEn = self['CONF']['InjEn'].tovalue()

        # disable Inj, enable RAMWr
        self['CONF']['InjEn'] = 0
        self['CONF']['RAMWrEnable'] = 1
        self._write_sr('CONF')

        for tbit, tmask in enumerate([1, 2, 4]):
            a = tdac[:, :] & tmask
            b = self.PixelConf['tdac'][:, :] & tmask
            rows = np.unique(np.argwhere(a != b)[:, 1])
            for r in rows:
                # set mask to TDAC_SR
                for c in range(self.ncols):
                    self['TDAC_CONF']['Col'][c] = '{0:d}'.format((tdac[c, r] & tmask) != 0)
                self._write_sr('TDAC_CONF')
                # select row
                for c in range(self.ncols):
                    self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'] = 0
                div_r, mod_r = divmod(r, 12)
                self['ROW_CONF']['ColRev'][self.ncols - div_r - 1]['EnRow'][mod_r] = '1'
                self._write_sr('ROW_CONF')
                # write RAM
                self['CONF']['WrRAM_TDAC'] = 1 << tbit
                self._write_sr('CONF')
                # log
                self.logger.info('set_tdac: row={0} bit={1} tdac=0x{2}'.format(
                    r, tbit, self['TDAC_CONF']['Col'].to01()))
                # set WrRAM to 0
                self['CONF']['WrRAM_TDAC'] = 0
                self._write_sr('CONF')
        # update tdac in class
        self.PixelConf['tdac'][:, :] = np.copy(tdac[:, :])
        # restore En Inj
        self['CONF']['InjEn'] = tmp_InjEn
        self['CONF']['RAMWrEnable'] = 0
        self._write_sr('CONF')
        # restore EnRow
        for c in range(self.ncols):
            self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'] = tmp_EnRow[c]
        self._write_sr('ROW_CONF')

    def init_PixelConf(self):
        tmp_InjEn = self['CONF']['InjEn'].tovalue()
        self['CONF']['InjEn'] = 0
        self['CONF']['RAMWrEnable'] = 1
        self._write_sr('CONF')

        # save Inj rows
        tmp_EnRow = []
        for c in range(self.ncols):
            tmp_EnRow.append(self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'].tovalue())

        # set all bits to 1 (TDAC_SR)
        self['TDAC_CONF']['Col'] = 0x1FFF_FFFF
        self._write_sr('TDAC_CONF')

        for r in range(self.nrows):
            # select row
            for c in range(self.ncols):
                self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'] = 0
            div_r, mod_r = divmod(r, 12)
            self['ROW_CONF']['ColRev'][self.ncols - div_r - 1]['EnRow'][mod_r] = '1'
            self._write_sr('ROW_CONF')

            self['CONF']['WrRAM_Comp'] = 1
            self['CONF']['WrRAM_TDAC'] = 7
            self._write_sr('CONF')
            # set WrRAM to 0
            self['CONF']['WrRAM_Comp'] = 0
            self['CONF']['WrRAM_TDAC'] = 0
            self._write_sr('ROW_CONF')

        self.PixelConf['tdac'][:, :] = 7
        self.PixelConf['comp_mask'][:, :] = 1
        # restore En Inj
        tmp_InjEn = self['CONF']['InjEn'].tovalue()
        self['CONF']['InjEn'] = tmp_InjEn
        self['CONF']['RAMWrEnable'] = 0
        self._write_sr('ROW_CONF')

        # restore EnRow
        for c in range(self.ncols):
            self['ROW_CONF']['ColRev'][self.ncols - c - 1]['EnRow'] = tmp_EnRow[c]
        self._write_sr('ROW_CONF')

        self['CONF']['RAMWrEnable'] = 0
        self._write_sr('CONF')

    def get_data_now(self):
        return self['fifo'].get_data()

    def get_data(self, timeout=1, n_ts=None):
        # rx on
        self['data_rx'].set_en(True)

        # inject
        if self["inj"].EN:
            self["gate"].start()
        else:
            self["inj"].start()

        # get nuumber of pulses
        if n_ts is None:
            n_ts = self['inj'].REPEAT
        nloops = int(timeout // 0.01)

        # get data
        raw = self['fifo'].get_data()
        for i in range(nloops):
            time.sleep(0.01)
            raw = np.append(raw, self['fifo'].get_data())
            if n_ts != 0 or self["inj"].is_ready:
                ts_len = (raw & 0xF000_0000 == 0x8000_0000).sum()
                break
        for j in range(i, nloops):
            time.sleep(0.01)
            tmp_raw = self['fifo'].get_data()
            raw = np.append(raw, tmp_raw)
            if n_ts != 0:
                ts_len = ts_len + (tmp_raw & 0xF000_0000 == 0x8000_0000).sum()
                if ts_len >= n_ts * 3:
                    break
        time.sleep(0.01)
        raw = np.append(raw, self['fifo'].get_data())

        # rx off
        self['data_rx'].set_en(False)
        time.sleep(0.01)
        raw = np.append(raw, self['fifo'].get_data())

        # check status
        idle = self["data_rx"].IDLE
        fifo_size = self['fifo'].FIFO_SIZE
        if idle != 0x1 or fifo_size != 0 or j == nloops - 1:
            self.logger.warn("get_data: error status=0x{0:x} fifo_size={1:d} loop={2:d} ts={3}/{4}".format(
                idle, fifo_size, j, ts_len, n_ts * 3))
        return raw

    def get_rx_status(self):
        ret = {'aligned': self['data_rx'].ALIGNED,
               'idle': self["data_rx"].IDLE,
               'bitslip': self["data_rx"].BITSLIP,
               'delay': self['data_rx'].ALIGNED,
               'lost_count': self["data_rx"].LOST_COUNT,
               }
        return ret

    def set_rx(self, en=True, reset_wait=0.1):
        self['SW']['EN_CkExt'] = en
        self['SW']['SYNC_RES'] = 1
        self['SW'].write()
        if en:
            self['data_rx'].reset()
            if self['fifo'].FIFO_SIZE != 0:  # Clear fifo
                self.logger.info('set_rx: FIFO is not empty({}), try to discard the data...'.format(self['fifo'].FIFO_SIZE))
                self['fifo'].get_data()
                if self['fifo'].FIFO_SIZE != 0:
                    self.logger.warn('set_rx: failed to empty FIFO({0})'.format(self['fifo'].FIFO_SIZE))
            cnt0 = int(reset_wait / 0.010)
            cnt = 0
            for i in range(cnt0 * 10):  # aligne data
                time.sleep(0.010)
                if self['data_rx'].ALIGNED:
                    cnt = cnt + 1
                if cnt > cnt0:
                    break
            if i == cnt0 * 10 - 1:
                self.logger.warn('set_rx: FPGA cannot align data: aligned={0}'.format(self['data_rx'].ALIGNED))
            self['SW']['SYNC_RES'] = 0
            self['SW'].write()
            self['sync'].START = 1
        self['data_rx'].set_en(en)

    def mask_rx(self, mask=True):
        self['data_rx'].set_en(not mask)
        if not mask:
            trash = self['fifo'].FIFO_SIZE
            if trash != 0:
                self['fifo'].get_data()
                self.logger.warn('mask_rx: discard trash ({0} {1})'.format(trash, self['fifo'].FIFO_SIZE))
            self['data_rx'].set_en(True)
        self.logger.info('mask_rx: mask={}'.format(mask))

    def reset_fifo(self):
        fifo_size = self['fifo'].FIFO_SIZE
        if fifo_size != 0:
            self.logger.info('reset_fifo: FIFO is not empty({}), discarding the data...'.format(fifo_size))
            self['fifo'].get_data()
            fifo_size = self['fifo'].FIFO_SIZE
            if fifo_size != 0:
                self.logger.warn('reset_fifo: FIFO cannot be empty({0})'.format(fifo_size))

    def set_ts_inj(self, en=True, reset=False):
        if en:
            self['ts_inj'].RESET = reset
            self['ts_inj'].LOST_COUNT = 0
        self['ts_inj'].ENABLE_RISING = en
        self.logger.info('set_ts_inj:enable={}, reset_timestamp={}'.format(self['ts_inj'].ENABLE_RISING, reset))

    def get_configuration(self):
        ret = super(TelePix, self).get_configuration()
        #ret['PixelConf'] = self.PixelConf
        # add some statuses
        ret['status'] = self.get_rx_status()
        ret['status']['inj_ready'] = self['inj'].READY
        ret['status']['sync_ready'] = self['sync'].READY
        ret['status']['gate_ready'] = self['gate'].READY
        ret['status']['ts_inj_clkdv'] = self['ts_inj'].CLKDV
        return ret

    def show_sync_status(self):
        self.logger.info('get_inj_status: sw={}'.format(self['SW'].get_configuration()))
        self.logger.info('get_inj_status: sync={} is_ready={}'.format(self['sync'].get_configuration(), self['sync'].is_ready))
        self.logger.info('get_inj_status: inj ={} is_ready={}'.format(self['inj'].get_configuration(), self['inj'].is_ready))
        self.logger.info('get_inj_status: gate={} is_ready={}'.format(self['gate'].get_configuration(), self['gate'].is_ready))

    def set_sync(self, nbits=7, force_reset_time=0.1, auto_sync=True):
        '''
           args
           nbits: ToA 12bits + neg, ToT 7bits, TDC 6bits
        '''
        # send manual sync
        self["tfc"].RESET = 1
        if force_reset_time != 0:
            self["tfc"]["BXRESET"] = 1
            self["tfc"].start()
        # configure automatic reset
        self['sync'].RESET = 1
        self['sync'].REPEAT = 0
        self['sync'].DELAY = 2**nbits - 1
        self['sync'].WIDTH = 1
        # update gate
        self['gate'].REPEAT = 1
        self['gate'].WIDTH = self['sync'].WIDTH + self['sync'].DELAY
        self['gate'].DELAY = 1
        # release force reset
        if force_reset_time > 0:
            time.sleep(force_reset_time)
            self["tfc"]["BXRESET"] = 0
            self["tfc"].start()
        # start auto sync
        self["tfc"]["BXRESET_PLS"] = auto_sync
        self["tfc"]["EXT_EN"] = auto_sync
        self['sync'].start()

        self.logger.info('set_sync: sync_period=0x{0:x} repeat={1} auto_sync={2}{3}{4} manual-sync={5}'.format(
            self['sync'].DELAY + self['sync'].WIDTH,
            self['sync'].REPEAT,
            self["tfc"]["BXRESET_PLS"], self["tfc"]["EXT_EN"], self["sync"].is_ready,
            self["tfc"]["BXRESET"]))


if __name__ == '__main__':
    pass
