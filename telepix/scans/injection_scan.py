import time

import numpy as np

import telepix.scan_base as scan_base
import telepix.analysis.interpreter as interpreter
import telepix.analysis.event_builder as event_builder

local_configuration = {
    "inj": None,  # np.arange(0.1,0.6,0.05),
    'inj_phase': None,
    "rows": np.arange(12),
    "cols": np.arange(29),
    "tdac": np.arange(7, -1, -1),
    "n_inj_col": 1,
    'n_inj_row': 1,
    "plsgen": None,
    "disable_noninjected_pixel": False,
}


class InjectionScan(scan_base.ScanBase):
    scan_id = "injection_scan"

    def scan(self, **kwargs):
        """ List of kwargs
            cols: list of pixel
            rows: list of pixel
            n_mask_pix: number of pixels injected at onece
            injlist: list of inj (inj_high-inj_low)
            with_mon: get timestamp of mon (mon will be enabled)
        """
        ####################
        # check inj_n
        if self.dut['inj'].REPEAT == 0:
            raise ValueError('injection should not be coutinuous inj_n={}'.format(self.dut['inj'].REPEAT))
        ####################
        # get options from args
        plsgen = kwargs.pop("plsgen", None)
        if plsgen is None:
            plsgen = self.dut
            is_plgen_external = False
        else:
            is_plgen_external = True
        disable_noninjected_pixel = kwargs.pop("disable_noninjected_pixel", local_configuration["disable_noninjected_pixel"])

        ####################
        # get scan params from args
        param_dtype = [("scan_param_id", "<i4"), ("timestamp", '<f8')]
        scan_param = {}
        nsteps = 1
        phaselist = kwargs.pop("inj_phase", local_configuration['inj_phase'])
        if phaselist is not None:
            param_dtype.append(('inj_phase', '<f4'))
            scan_param['inj_phase'] = phaselist
            nsteps = nsteps * len(scan_param['inj_phase'])

        injlist = kwargs.pop("inj", local_configuration['inj'])
        if injlist is not None:
            if isinstance(injlist, float):
                injlist = np.array([injlist])
            elif isinstance(injlist, list):
                injlist = np.array(injlist)
            param_dtype.append(('inj', '<f4'))
            scan_param['inj'] = injlist
            nsteps = len(scan_param['inj'])

        tdaclist = kwargs.pop('tdac', None)
        if tdaclist is not None:
            if isinstance(tdaclist, int):
                tdaclist = np.array([tdaclist])
            elif isinstance(tdaclist, list):
                tdaclist = np.array(tdaclist)
            param_dtype.append(('tdac', '<u1'))
            scan_param['tdac'] = tdaclist
            nsteps = nsteps * len(scan_param['tdac'])
            tdac = np.copy(self.dut.PixelConf['tdac'])
            tdac_org = np.copy(self.dut.PixelConf['tdac'])

        rows = kwargs.pop("rows", local_configuration['rows'])
        if rows is not None:
            if isinstance(rows, int):
                rows = np.array([rows], dtype=np.int8)
            n_inj_row = min(kwargs.pop("n_inj_row", local_configuration['n_inj_row']), len(rows))
            n_loop_row = (len(rows) - 1) // n_inj_row + 1
            rowlist = np.ones([n_loop_row, n_inj_row], dtype='<u2') * 0xFFFF
            for i, r in enumerate(rows):
                div, mod = divmod(i, n_loop_row)
                rowlist[mod, div] = r
            param_dtype.append(("rows", "<u2", (n_inj_row)))
            scan_param['rows'] = rowlist
            nsteps = nsteps * n_loop_row
            rows = rowlist[0, :]
        cols = kwargs.pop("cols", local_configuration['cols'])  # TODO set en_inj_col without rows
        if cols is not None:
            if isinstance(cols, int):
                cols = np.array([cols], dtype=np.int8)
            n_inj_col = min(kwargs.pop("n_inj_col", local_configuration['n_inj_col']), len(cols))
            n_loop_col = (len(cols) - 1) // n_inj_col + 1
            collist = np.ones([n_loop_col, n_inj_col], dtype='<u1') * 0xFF
            for i, c in enumerate(cols):
                div, mod = divmod(i, n_loop_col)
                collist[mod, div] = c
            param_dtype.append(("cols", "<u1", (n_inj_col)))
            scan_param['cols'] = collist
            nsteps = nsteps * n_loop_col
            cols = collist[0, :]

        for k in kwargs:
            param_dtype.append((k, '<u2'))
            scan_param[k] = kwargs[k]
            nsteps = nsteps * len(scan_param[k])

        ####################
        # create a table for scan_params
        param_dtype = np.dtype(param_dtype)
        self.scan_param_table = self.h5_file.create_table(
            self.h5_file.root,
            name='scan_parameters',
            title='scan_parameters',
            description=param_dtype,
            filters=self.filter_tables)
        for k, v in scan_param.items():
            self.meta_data_table.attrs[k] = v

        ####################
        # enable readout
        self.dut.set_ts_inj(True)
        self.dut.set_rx(True)
        self.dut.mask_rx(True)
        ts_n = self.dut['inj']['REPEAT']
        with self.readout(scan_param_id=0,                       # TODO open/close self.readout for each scan step (make it faster)
                          fill_buffer=True, clear_buffer=True,
                          reset_rx=False, reset_sram_fifo=False,  # set_rx, does these resets
                          readout_interval=0.001):
            dqdata = self.fifo_readout.data
            ####################
            # main scan loop
            self.logger.info('starting scan nsteps={}'.format(nsteps))
            for scan_param_id in range(nsteps):
                # update scan parameters
                self.scan_param_table.row['scan_param_id'] = scan_param_id
                scan_div = 1
                conf_kw = {}
                rx_reset = 0
                for pname, plist in scan_param.items():
                    plen = len(plist)
                    mod = (scan_param_id // scan_div) % plen
                    self.scan_param_table.row[pname] = plist[mod]
                    if scan_param_id // scan_div != (scan_param_id - 1) // scan_div:
                        self.logger.debug(scan_param_id, 'scan_div=', scan_div, 'plen=', plen, 'mod=', mod, end=' ')
                        if pname == 'inj':
                            if not is_plgen_external or plist[mod] < plsgen.SET['high']:
                                self.dut.mask_rx(True)
                            plsgen.set_inj_amp(plist[mod], unit='V')
                        elif pname == 'inj_phase':
                            plsgen.set_inj_phase(inj_phase=plist[mod])
                        elif pname == 'rows':
                            rows = plist[mod][plist[mod] != 0xFFFF]
                            self.dut.mask_rx(True)
                            print('-------------rows', rows)
                            self.dut.set_en_inj(cols=None, rows=rows)
                            rx_reset = 1
                        elif pname == 'cols':
                            cols = plist[mod][plist[mod] != 0xFF]
                            self.dut.mask_rx(True)
                            print('-------------cols', cols)
                            self.dut.set_en_inj(cols=cols, rows=None)
                            rx_reset = 1
                        elif pname == 'tdac':
                            tdac[:, :] = tdac_org[:, :]
                            for r in rows:
                                for c in cols:
                                    tdac[c, r] = plist[mod]
                            self.dut.mask_rx(True)
                            self.dut.set_tdac(tdac, comp=0)
                            rx_reset = 1
                        else:
                            conf_kw[pname] = int(plist[mod])
                    scan_div = scan_div * plen
                if len(conf_kw) != 0:
                    self.dut.mask_rx(True)
                    self.dut.set_conf(**conf_kw)
                    rx_reset = 1
                self.scan_param_table.row['timestamp'] = time.time()
                self.scan_param_table.row.append()
                self.scan_param_table.flush()

                # inject pulse
                if rx_reset:
                    self.dut.set_rx(True)
                self.dut.mask_rx(False)
                self.scan_param_id = scan_param_id
                self.dut.inject()

                # count ts-data
                while True:
                    time.sleep(0.01)
                    try:
                        data = np.concatenate([item[0] for item in dqdata])
                        ts_len = (data & 0xF000_0000 == 0x8000_0000).sum()
                        if ts_len >= ts_n * 3:
                            break
                    except ValueError:
                        pass
                # time.sleep(0.05)
                try:
                    data = np.concatenate([item[0] for item in dqdata])
                    hit_len = (data & 0xF000_0000 == 0x1000_0000).sum()
                except ValueError:
                    pass
                self.logger.info('InjectionScan: step={} ts={} hit={}'.format(scan_param_id, ts_len, hit_len))

                # mask rx for next step
                self.fifo_readout._data_buffer.clear()

    def analyze(self):
        fraw = self.output_filename + '.h5'
        fhit = fraw[:-7] + 'hit.h5'

        # interpret and event_build
        interpreter.interpret_h5(fraw, fhit)
        self.logger.info('interpreted {}'.format(fhit))
        event_builder.build_h5(fraw=fraw, fhit=fhit)
        self.logger.info('alingned to timestamp {}'.format(fhit))
        return fhit

    def plot(self):
        fraw = self.output_filename + '.h5'
        fhit = self.output_filename[:-4] + 'hit.h5'
        fpdf = self.output_filename + '.pdf'


if __name__ == "__main__":
    from telepix.telepix import telepix
    dut = telepix()
    dut.init()
    dut.set_en_comp('all')
    #dut.set_sync(inj)
    dut.set_inj(inj_n=100, ext=True)
    dut.set_inj_amp(0.1, unit='V')

    scan = InjectionScan(dut, online_monitor_addr="")
    scan.start(**local_configuration)
    scan.analyze()
    #scan.plot()
