import numpy as np


def load_msohist(fname):
    dat = open(fname).readlines()
    tmp = dat[0].split(";")
    b = float(tmp[-2])
    e = float(tmp[-1])
    dat = dat[1].split(",")
    x = np.arange(b, e, (e - b) / len(dat))
    return np.array([x, dat], float)


def load_msohist2(fname):
    dat = open(fname).readlines()
    tmp = dat[0].split(";")
    b = float(tmp[-2])
    e = float(tmp[-1])
    dat = dat[2].split(",")
    x = np.arange(b, e, (e-b)/len(dat))
    return np.array([x,dat],float)


def load_mso(fname):
    dat = open(fname).readlines()
    ret = []
    dl = 10000
    for j, d_line in enumerate(dat):
        ret.append({})
        for dd in d_line.split(":"):
            d = dd.split(";")
        # if len(d[-1]) < 2:
        #     i_dat = len(d) - 2
        # else:
        #     i_dat = len(d) - 1
            i = 0
            while i < len(d):
                #print(i, d[i][:100])
                if "Sample mode" in d[i]:
                    ch = d[i][1:].split(",")[0][-3:]
                    dl = int(d[i + 1].split()[-1])
                    i = i + 2
                elif '"s"' in d[i]:
                    xinc = float(d[i + 1].split()[-1])
                    xzero = float(d[i + 2].split()[-1])
                    x = np.arange(xzero, xinc * (dl) + xzero, xinc)
                    ret[-1]["Time"] = x[:dl]
                    i = i + 3
                elif '"V"' in d[i]:
                    yinc = float(d[i + 1].split()[-1])
                    yoff = float(d[i + 2].split()[-1])
                    yzero = float(d[i + 3].split()[-1])
                    i = i + 4
                elif len(d[i]) > dl:
                    if 'CURVE ' in d[i]:
                        d[i] = d[i][len('CURVE '):]
                    #ry:
                    ret[-1][ch] = (np.array(d[i].split(","), float) - yoff) * yinc + yzero
                    #except:
                    #    print("ERROR", "line = ", j, len(d), d_line)
                    #    return ret
                    i = i + 1
                else:
                    i = i + 1
    return ret