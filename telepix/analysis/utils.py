import numpy as np
import tables as tb
import yaml
import bitarray


def load_msohist(fname):
    dat = open(fname).readlines()
    tmp = dat[0].split(";")
    b = float(tmp[-2])
    e = float(tmp[-1])
    dat = dat[1].split(",")
    x = np.arange(b, e, (e - b) / len(dat))
    return np.array([x, dat], float)


def load_msohist2(fname):
    dat = open(fname).readlines()
    tmp = dat[0].split(";")
    b = float(tmp[-2])
    e = float(tmp[-1])
    dat = dat[2].split(",")
    x = np.arange(b, e, (e-b)/len(dat))
    return np.array([x,dat],float)


def load_mso(fname):
    dat = open(fname).readlines()
    ret = []
    dl = 10000
    for j, d_line in enumerate(dat):
        ret.append({})
        for dd in d_line.split(":"):
            d = dd.split(";")
        # if len(d[-1]) < 2:
        #     i_dat = len(d) - 2
        # else:
        #     i_dat = len(d) - 1
            i = 0
            while i < len(d):
                if "Sample mode" in d[i]:
                    ch = d[i][1:].split(",")[0][-3:]
                    dl = int(d[i + 1].split()[-1])
                    i = i + 2
                elif '"s"' in d[i]:
                    xinc = float(d[i + 1].split()[-1])
                    xzero = float(d[i + 2].split()[-1])
                    x = np.arange(xzero, xinc * (dl) + xzero, xinc)
                    ret[-1]["Time"] = x[:dl]
                    i = i + 3
                elif '"V"' in d[i]:
                    yinc = float(d[i + 1].split()[-1])
                    yoff = float(d[i + 2].split()[-1])
                    yzero = float(d[i + 3].split()[-1])
                    i = i + 4
                elif len(d[i]) > dl:
                    if 'CURVE ' in d[i]:
                        d[i] = d[i][len('CURVE '):]
                    #ry:
                    ret[-1][ch] = (np.array(d[i].split(","), float) - yoff) * yinc + yzero
                    #except:
                    #    print("ERROR", "line = ", j, len(d), d_line)
                    #    return ret
                    i = i + 1
                else:
                    i = i + 1
    return ret


def load_config(fraw, attr='firmware_before'):
    with tb.open_file(fraw) as f:
        if attr is None:
            return f.root.meta_data.attrs._f_list('user')
        else:
            tmp_y = f.root.meta_data.attrs[attr]
    try:
        ret = yaml.safe_load(tmp_y)
    except:
        tmp_y = tmp_y.split('\n')
        flg = 1
        tmp = []
        for ty in tmp_y:
            if ty == 'PixelConf:':
                flg = 0
            elif ty == 'ROW_CONF:':
                flg = 1
            if flg:
                tmp.append(ty)
            else:
                pass
        tmp_y = '\n'.join(tmp)
        ret = yaml.safe_load(tmp_y)

    if 'firmware' in attr:
        for k in ret['CONF']:
            if k in ['INFB']:
                tmp_v = bitarray.bitarray(ret['CONF'][k])
                tmp_v.reverse()
                ret['CONF'][k] = int(tmp_v.to01(), base=2)
    return ret


def e2plgen(e):
    a = 0.051023722530250684
    b = 0.013189965291250544
    fe_ampout = 0.07450071206162819
    return ((fe_ampout / 1640 * e) - b) / a


def plgen2e(pls):
    a = 0.051023722530250684
    b = 0.013189965291250544
    fe_ampout = 0.07450071206162819
    return 1640 / fe_ampout*(a * pls + b)