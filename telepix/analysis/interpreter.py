import time
import numpy as np
from numba import njit
import tables
import logging

logger = logging.getLogger()


hit_dtype = [("event_number", "<i8"), ("col", "<u1"), ("row", "<u2"),
             ("ts", "<u2"), ("ts_n", "<u2"), ("ts2", "<u1"), ("ts3", "<u1"),
             ("timestamp", "<u2"), ('err', '<u1')]
ts_dtype = [("event_number", "<i8"), ("timestamp", "<i8"), ('err', '<u1')]


@njit
def decode_gray(gray, nbits):
    binary = gray & (1 << (nbits - 1))
    for i in range(nbits - 2, -1, -1):
        binary = binary | ((gray ^ (binary >> 1)) & (1 << i))
    return binary


@njit
def _interpret(raw, buf, buf_ts, idx_offset=0):
    idx = 0
    r_i = 0
    t_i = -1
    while idx < len(raw):
        # print(idx, '0x{0:x}: t_i {1}'.format(raw[idx], t_i), ':', hex(buf[t_i]['timestamp']), buf[t_i])
        # ################
        # inj timestamp
        if (raw[idx] & 0xF000_0000) == 0x8000_0000:
            # print('--', idx, '0x{0:x}: t_i {1}'.format(raw[idx], t_i), ':', buf_ts[t_i])
            if raw[idx] & 0x0F00_0000 == 0x300_0000:
                t_i = t_i + 1
                if t_i >= len(buf_ts):
                    return r_i, t_i, idx + 1
                buf_ts[t_i]['timestamp'] = np.int64(raw[idx] & 0x00FF_FFFF) << (48 - 4)
                buf_ts[t_i]['err'] = 0x3
                buf_ts[t_i]['event_number'] = idx + idx_offset
            elif buf_ts[t_i]['err'] == 0x3:
                if raw[idx] & 0x0F00_0000 == 0x200_0000:
                    # print('2', hex(buf_ts[t_i]['timestamp']))
                    buf_ts[t_i]['timestamp'] = buf_ts[t_i]['timestamp'] | (np.int64(raw[idx] & 0x00FF_FFFF) << (24 - 4))
                    buf_ts[t_i]['err'] = 0x2
                    # print('2', hex(buf_ts[t_i]['timestamp']))
                else:
                    buf_ts[t_i]['timestamp'] = np.int64(raw[idx])
            elif buf_ts[t_i]['err'] == 0x2:
                if raw[idx] & 0x0F00_0000 == 0x100_0000:
                    # print('1', hex(buf_ts[t_i]['timestamp']))
                    buf_ts[t_i]['timestamp'] = buf_ts[t_i]['timestamp'] | (np.int64(raw[idx] & 0x00FF_FF00) >> 4) | np.int64(raw[idx] & 0x0F)
                    buf_ts[t_i]['err'] = 0x0
                    # print('1', hex(buf_ts[t_i]['timestamp']))
                else:
                    buf_ts[t_i]['timestamp'] = np.int64(raw[idx])
        # ################
        # DUT data
        elif (raw[idx] & 0xE000_0000) == 0x0000_0000:
            # print('++', idx, '0x{0:x}: r_i {1}'.format(raw[idx], r_i), ':', buf[r_i])
            if buf[r_i]['err'] == 0xF:
                if (raw[idx] &                     0b0001_1000_0000_0000_0001_1100_0000_0111) == 0x0800_0000:  # noqa
                    buf[r_i]['err'] = 0x1
                    buf[r_i]['event_number'] = idx
                    buf[r_i]['ts2'] = (raw[idx] &                             0b11_1111_1000) >> 3   # 7bits  # noqa
                    buf[r_i]['ts2'] = decode_gray(buf[r_i]['ts2'], 7)
                    buf[r_i]['col'] = ((raw[idx] &              0b1_1111_1110_0000_0000_0000) >> 13) - 2  # 8bits  # noqa
                    if buf[r_i]['col'] > 0x7F:
                        buf[r_i]['col'] = (~buf[r_i]['col']-2 & 0xFF) + 1
                    buf[r_i]['row'] = (~raw[idx] &       0b111_1110_0000_0000_0000_0000_0000) >> 21  # noqa
                else:
                    buf[r_i]['ts'] = raw[idx] & 0xFFFF
                    buf[r_i]['ts_n'] = (raw[idx] & 0xFFFF_0000) >> 16
                    buf[r_i]['err'] = 0xE
                    r_i = r_i + 1
                    buf[r_i]['err'] = 0xF
            elif buf[r_i]['err'] == 0x1:
                if (raw[idx] &                    0b1111_1000_0000_0000_0000_0000_0001_1000) == 0x1000_0010:  # noqa
                    buf[r_i]['err'] = 0x2
                    buf[r_i]['row'] = buf[r_i]['row'] | ((~raw[idx] &                 0b111) << 6)  # 9bits  # noqa
                    buf[r_i]['ts3'] = (raw[idx] &                          0b0111_1110_0000) >> 5  # 6bits  # noqa
                    buf[r_i]['ts3'] = decode_gray(buf[r_i]['ts3'], 6)
                    buf[r_i]['ts_n'] = (raw[idx] &           0b111_1111_1111_1000_0000_0000) >> 11  # 12bits  # noqa
                    buf[r_i]['ts_n'] = decode_gray(buf[r_i]['ts_n'], 12)
                    buf[r_i]['ts'] = (raw[idx] &        0b111_1000_0000_0000_0000_0000_0000) >> 23  # noqa
                else:
                    buf[r_i]['event_number'] = idx
                    buf[r_i]['ts'] = raw[idx] & 0xFFFF
                    buf[r_i]['ts_n'] = (raw[idx] & 0xFFFF_0000) >> 16
                    buf[r_i]['err'] = 0xD
                    r_i = r_i + 1
                    buf[r_i]['err'] = 0xF
            elif buf[r_i]['err'] == 0x2:
                if (raw[idx] &                    0b1111_1100_0000_0000_0000_0011_0000_0000) == 0x1800_0200:  # noqa
                    buf[r_i]['err'] = 0
                    buf[r_i]['ts'] = buf[r_i]['ts'] | ((raw[idx] &              0b1111_1111) << 4)  # 12bits  # noqa
                    buf[r_i]['ts'] = decode_gray(buf[r_i]['ts'], 12)
                    buf[r_i]['timestamp'] = (raw[idx] & 0b011_1111_1111_1111_1100_0000_0000) >> 8   # 16bits  # noqa
                    r_i = r_i + 1
                    buf[r_i]['err'] = 0xF
                else:
                    buf[r_i]['event_number'] = idx
                    buf[r_i]['ts'] = raw[idx] & 0xFFFF
                    buf[r_i]['ts_n'] = (raw[idx] & 0xFFFF_0000) >> 16
                    buf[r_i]['err'] = 0xC
                    r_i = r_i + 1
                    buf[r_i]['err'] = 0xF
        idx = idx + 1
    return r_i, t_i, idx


class InterRaw():
    def __init__(self, chunk=100000, debug=0):
        self.buf = np.empty(chunk, dtype=hit_dtype)
        self.buf_ts = np.empty(chunk, dtype=ts_dtype)
        self.r_i = 0
        self.state = 0
        self.buf[0]['err'] = 0xF
        self.t_i = -1
        self.chunk = chunk
        self.debug = debug

    def reset(self):
        if self.state == 1:
            self.t_i = -1
        elif self.state & 0x4 != 0:
            self.buf_ts[0] = self.buf_ts[self.t_i]
            self.t_i = 0

        if self.state & 0x2 != 0:
            info = np.uint(self.r_i & 0xF0)
            self.buf[0] = self.buf[-1]
            if self.buf[0]['err'] != 0:
                byte_stat = np.uint(0xF)
            else:
                byte_stat = np.uint(0)
            self.buf[0]['err'] = info | byte_stat
        else:
            self.buf[0] = self.buf[self.r_i]
        self.r_i = 0

        self.state = 0

    def run(self, raw, idx_offset=0):
        self.r_i, self.t_i, idx = _interpret(raw, self.buf, self.buf_ts, idx_offset=idx_offset)
        if len(raw) != idx:
            if self.t_i >= len(self.buf_ts):
                logger.info('ts_buffer is full')
                self.state = 1
                return self.buf[:self.r_i], self.buf_ts, idx + idx_offset
            else:
                logger.info('hit_buffer is full')
                if self.t_i != -1 and self.buf_ts[self.t_i]['err'] != 0:
                    print('imcomplete ts', self.buf_ts[self.t_i])
                    self.state = 4 + 2
                    return self.buf, self.buf_ts[:self.t_i], idx + idx_offset
                else:
                    self.state = 2
                    return self.buf, self.buf_ts[:self.t_i + 1], idx + idx_offset

        if (self.buf[self.r_i]['err'] & 0x0F) not in [0, 0xF]:
            print('imcomplete hit', self.buf[self.r_i]['err'] & 0x0F, self.buf[self.r_i])

        if self.t_i != -1 and self.buf_ts[self.t_i]['err'] != 0:
            print('imcomplete ts', self.buf_ts[self.t_i])
            self.state = 4
            return self.buf[:self.r_i], self.buf_ts[:self.t_i], idx + idx_offset
        else:
            # print('completed ts', self.buf_ts[self.t_i])
            self.state = 0
            return self.buf[:self.r_i], self.buf_ts[:self.t_i + 1], idx + idx_offset

    def mk_list(self, raw):
        dat = self.run(raw)
        self.reset()
        return dat


def interpret_h5(fin, fout, chunk=10_000_000):
    with tables.open_file(fout, "w") as f_o, tables.open_file(fin) as f_i:
        hit_table = f_o.create_table(f_o.root,
                                     name="hit_data",
                                     description=np.dtype(hit_dtype),
                                     title='hit_data')
        ts_table = f_o.create_table(f_o.root,
                                    name="ts_data",
                                    description=np.dtype(ts_dtype),
                                    title='ts_data')
        end = len(f_i.root.raw_data)
        start = 0
        inter = InterRaw(chunk=min(end, chunk // 2))
        t0 = time.time()
        while start < end:
            tmpend = min(end, start + chunk)
            hit, ts, start = inter.run(f_i.root.raw_data[start:tmpend], idx_offset=start)
            hit_table.append(hit)
            hit_table.flush()
            ts_table.append(ts)
            ts_table.flush()
            print('{0:.2f}s {1:d}/{2:d} hit={3} ts={4}'.format(time.time() - t0, tmpend, end,
                                                               hit_table.shape[0], ts_table.shape[0],))
            inter.reset()


def raw2list(raw):
    inter = InterRaw(chunk=len(raw) + 10)
    hit, ts, idx = inter.run(raw)
    if idx != len(raw):
        logger.warn('buffer was too small len_raw={0} interpreted-idx={1}'.format(len(raw), idx))
    return hit, ts


if __name__ == "__main__":
    import sys
    fin = sys.argv[1]
    fout = fin[:-3] + "_hit.h5"
    interpret_h5(fin, fout, debug=3)
