import numpy as np
from numba import njit
import tables
import time
import logging

logger = logging.getLogger()

event_dtype = [('event_number', '<i8'), ('col', '<u1'), ('row', '<u2'),
               ('toa', '<u2'), ('tot', '<u2'), ('tdc', '<u1'),
               ('err', 'u1'), ('timestamp', '<i8')]


#@njit
def _build(hit, ts, buf, idx_offset, end, del_hit_err=True):
    t_i = 0
    h_i = 0
    err = 0
    # print('len', len(hit), len(ts))
    for h in hit:
        if h['err'] != 0:
            err = err + 1
            if del_hit_err:
                continue
        buf[h_i]['col'] = h['col']
        buf[h_i]['row'] = h['row']
        buf[h_i]['toa'] = h['ts'] + h['ts_n']
        if h['ts'] == 0 and h['ts_n'] == 127:
            buf[h_i]['toa'] = 255
        else:
            buf[h_i]['toa'] = h['ts'] + h['ts_n']
        buf[h_i]['tot'] = ((h['ts2'] << 1) - buf[h_i]['toa']) & 0xFF
        buf[h_i]['tdc'] = ((h['ts3'] << 1) - buf[h_i]['toa']) & 0x7F
        # print('t_i', t_i, h['event_number'], len(ts[t_i:]))
        for i, t in enumerate(ts[t_i:]):
            if t['event_number'] > h['event_number']:
                break
        if t['event_number'] > h['event_number']:
            t_i = max(0, t_i + i - 1)
        else:
            t_i = t_i + i
            if end is not True:
                return h_i, t_i, err
        buf[h_i]['err'] = ts[t_i]['err']
        buf[h_i]['event_number'] = t_i + idx_offset
        buf[h_i]['timestamp'] = ts[t_i]['timestamp']
        h_i = h_i + 1
    return h_i, t_i, err


@njit
def _build_with_meta(hit, ts, buf, meta, end, del_hit_err=True):
    t_i = 0
    h_i = 0
    m_i = 0
    err = 0
    # print('len', len(hit), len(ts))
    for h in hit:
        if h['err'] != 0:
            err = err + 1
            if del_hit_err:
                continue
        buf[h_i]['col'] = h['col']
        buf[h_i]['row'] = h['row']
        if h['ts'] == 0 and h['ts_n'] == 127:
            buf[h_i]['toa'] = 255
        else:
            buf[h_i]['toa'] = h['ts'] + h['ts_n']
        buf[h_i]['tot'] = (h['ts2'] << 1) - buf[h_i]['toa'] & 0xFF
        buf[h_i]['tdc'] = (h['ts3'] << 1) - buf[h_i]['toa'] & 0x7F
        # print('t_i', t_i, h['event_number'], len(ts[t_i:]))
        for i, t in enumerate(ts[t_i:]):
            if t['event_number'] > h['event_number']:
                break
        if t['event_number'] > h['event_number']:
            t_i = max(0, t_i + i - 1)
        else:
            t_i = t_i + i
            if end is not True:
                return h_i, t_i, err
        buf[h_i]['err'] = ts[t_i]['err']
        buf[h_i]['timestamp'] = ts[t_i]['timestamp']

        for i, m in enumerate(meta[m_i:]):
            if m['index_stop'] >= h['event_number'] and m['index_start'] <= h['event_number']:
                buf[h_i]['event_number'] = np.uint64(m['scan_param_id'])
                break
        m_i = max(0, m_i + i)
        if m_i == len(meta)-1:
            if m['index_stop'] >= h['event_number'] and m['index_start'] <= h['event_number']:
                pass
            else:
                m_i = 0
                buf[h_i]['err'] = 0xEF
        h_i = h_i + 1
    return h_i, t_i, err


class BuildHit():
    def __init__(self, chunk=100000, idx_offset=0):
        self.buf = np.empty(chunk, dtype=event_dtype)
        self.t_i = 0
        self.idx_offset = idx_offset

    def reset(self):
        self.idx_offset = self.t_i
        self.h_i = 0
        self.t_i = 0

    def run(self, hit, ts, meta=None, end=True):
        if meta is None:
            self.hit_i, self.ts_i, self.err = _build(hit, ts, self.buf, self.idx_offset, end=end)
        else:
            self.hit_i, self.ts_i, self.err = _build_with_meta(hit, ts, self.buf, meta, end=end)

        if self.err != 0:
            logger.info('BuildHit.run() n of error_hit={}'.format(self.err))
        return self.buf[:self.hit_i]


def hit2ev(hit, ts, idx_offset=0):
    eb = BuildHit(len(hit), idx_offset)
    return eb.run(hit, ts)


def build_h5(fraw, fhit, chunk=10_000_000):
    with tables.open_file(fraw) as f:
        meta = f.root.meta_data[:]
    with tables.open_file(fhit, 'a') as f:
        if f.__contains__('/event_data'):
            f.root.event_data.remove()
        event_table = f.create_table(
            f.root,
            name="event_data",
            description=np.dtype(event_dtype),
            title='event_data')

        end = len(f.root.hit_data)
        end_ts = len(f.root.ts_data)
        start = 0
        start_ts = 0
        t0 = time.time()
        builder = BuildHit(chunk=min(end, chunk))

        while start < end:
            tmpend = min(end, start + chunk)
            if tmpend != end:
                print("WARNINIG WARNINIG WARNINIG WARNINIG debug this before using")
            hit = f.root.hit_data[start: tmpend]

            while True:
                tmpend_ts = min(end_ts, start + chunk)
                if tmpend_ts == end_ts:
                    end_flg = True
                else:
                    end_flg = False
                    print("TS WARNINIG WARNINIG WARNINIG WARNINIG debug this before using")
                ts = f.root.ts_data[start_ts: tmpend_ts]
                event_table.append(builder.run(hit, ts, meta, end=end_flg))
                event_table.flush()
                start_ts = start_ts + builder.ts_i
                if builder.hit_i + builder.err == len(hit):
                    break
                else:
                    hit = hit[builder.hit_i:]
            start = start + len(hit)
            logger.info('{0:.2f}s hit={1:d}/{2:d} event={3} ts={4}/{5}'.format(
                time.time() - t0, tmpend, end,
                event_table.shape[0], tmpend_ts, end_ts))
