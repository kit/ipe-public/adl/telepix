import numpy as np
from scipy.special import erf
from scipy.optimize import curve_fit  # , leastsq
import matplotlib.pyplot as plt


def scurve(x, A, mu, sigma):
    return 0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


def scurve_rev(x, A, mu, sigma):
    return 0.5 * A * erf((mu - x) / (np.sqrt(2) * sigma)) + 0.5 * A


def gauss(x, A, mu, sigma):
    return A * np.exp(-(x - mu) ** 2.0 / (2.0 * sigma ** 2.0))



def line(x, a, b):
    return a * x + b


def fit_gauss(x_data, y_data):
    """Fit gauss"""
    x_data = np.array(x_data)
    y_data = np.array(y_data)
    y_maxima = x_data[np.where(y_data[:]==np.max(y_data))[0]]
    params_guess = np.array([np.max(y_data), y_maxima[0], np.std(x_data)])
    try:
        p, cov = curve_fit(gauss, x_data, y_data, p0=params_guess, maxfev=1000)
    except RuntimeError:
        print('Fit did not work (gauss): %s %s %s', str(np.max(y_data)), str(x_data[np.where(y_data[:] == np.max(y_data))[0]][0]), str(np.std(x_data)))
        return params_guess[0],params_guess[1],params_guess[2]
    err = np.sqrt(np.diag(cov))
    return p[0], p[1], p[2], err[0], err[1], err[2]


def fit_line(xarray, yarray):
    if len(xarray) < 1:
        return np.nan, np.nan, np.nan, np.nan
    p, cov = curve_fit(line, xarray, yarray)
    err = np.sqrt(np.diag(cov))
    return p[0], p[1], err[0], err[1]


def parabola(x, a, b, c):
    return a * x * x + b * x + c


def fit_parabola(xarray, yarray):
    if len(xarray) < 1:
        return np.nan, np.nan, np.nan, np.nan
    p, cov = curve_fit(parabola, xarray, yarray)
    err = np.sqrt(np.diag(cov))
    return p[0], p[1], p[2], err[0], err[1], err[2]

def fit_scurve(xarray, yarray, A=None, cut_ratio=0.05, reverse=True, debug=0):
    if reverse:
        arg = np.argsort(xarray)[::-1]
    else:
        arg = np.argsort(xarray)
    yarray = yarray[arg]
    xarray = xarray[arg]
    if debug == 1:
        plt.plot(xarray, yarray, ".")
    # estimate
    if A is None:
        A = np.max(yarray)
    mu = xarray[np.argmin(np.abs(yarray - A * 0.5))]
    try:
        sig2 = xarray[np.argwhere(yarray > A * cut_ratio)[0]][0]
        sig1 = xarray[np.argwhere(yarray > A * (1 - cut_ratio))[0]][0]
        sigma = abs(sig1 - sig2) / 3.5
    except:
        sigma = 1
    if debug == 1:
        print("estimation", A, mu, sigma)
    # cut
    cut_high = np.argwhere(yarray >= A * (1 + cut_ratio))
    cut_low = np.argwhere(yarray >= A * (1 - cut_ratio))
    if len(cut_high) > 0:
        cut = cut_high[:, 0][0]
    else:
        cut = len(yarray)
    if len(cut_low) > 0:
        cut = min(cut, cut_low[:, 0][-1])
    yarray = yarray[:cut]
    xarray = xarray[:cut]
    if debug == 1:
        plt.plot(xarray, yarray, "o")
        plt.plot(xarray, scurve_rev(xarray, A, mu, sigma), "--")
    try:
        if reverse:
            p, cov = curve_fit(scurve_rev, xarray, yarray, p0=[A, mu, sigma])
        else:
            p, cov = curve_fit(scurve, xarray, yarray, p0=[A, mu, sigma])
    except RuntimeError:
        if debug == 2:
            print('fit did not work')
        return A, mu, sigma, float("nan"), float("nan"), float("nan")
    err = np.sqrt(np.diag(cov))
    return p[0], p[1], p[2], err[0], err[1], err[2]


def find_arg(a, th, dec=False):
    tmp = np.argwhere(a > th)
    if dec:
        if len(tmp) == 0:
            high = 1
            low = 0
            arg = (th - a[high]) * (high - low) / (a[high] - a[low]) + high
        else:
            if tmp[-1][0] == len(a) - 1:
                high = tmp[-1][0]
                low = high - 1
                arg = (th - a[high]) * (high - low) / (a[high] - a[low]) + high
            else:
                high = tmp[-1][0]
                low = high + 1
                arg = (th - a[high]) * (high - low) / (a[high] - a[low]) + high
    else:
        if len(tmp) == 0:
            high = -1
            arg = (th - a[high]) / (a[high] - a[high - 1]) + high
        else:
            if tmp[0][0] == 0:
                high = 1
                arg = (th - a[0]) / (a[high] - a[0]) + 0
            else:
                high = tmp[0][0]
                arg = (th - a[high]) / (a[high] - a[high - 1]) + high
    return arg


def find_value(a, arg):
    if arg <= 0:
        return a[0]
    if arg >= len(a) - 1:
        return a[-1]
    else:
        high = int(np.ceil(arg))
        low = int(high - 1)
        return (a[high] - a[low]) * (arg - float(high)) + a[high]